package miuoshki.ossim.domain.paging_in_processor;

import miuoshki.ossim.domain.Page;
import miuoshki.ossim.domain.paging_in_process.LRU;

import miuoshki.ossim.domain.Process;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class WorkingSet extends FrameAllocationAlgorithm {

    private static int delta = 10;
    private HashMap<Process, Queue> history = new HashMap<>();

    public static void setDelta(int delta) {
        WorkingSet.delta = delta;
    }

    public WorkingSet(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    public int calcFramesForProcess(Process process) {
        int framesToAllocate = history.get(process).uniqueSize();

        if (framesToAllocate <= availableFrames) {
            history.get(process).add(process.getProcessesIncomingPages().get(0));
        }

        return (framesToAllocate < process.getMinFrames()) ? process.getMinFrames() : framesToAllocate;
    }

    @Override
    protected void initializeMaps(List<Process> processes) {
        for (Process process : processes) {
            workingProcesses.put(process, new LRU());
            history.put(process, new Queue());
        }
    }

    @Override
    public String toString() {
        return "Working Set";
    }

    public static class Queue {

        List<Page> processesHistory = new LinkedList<>();

        public void add(Page page) {
            if (processesHistory.size() == delta) {
                processesHistory.remove(0);
            }
            processesHistory.add(page);
        }

        public int uniqueSize() {
            return (int)processesHistory.stream().map(Page::getNumber).distinct().count();
        }

    }

}
