package miuoshki.ossim.domain.paging_in_processor;

import miuoshki.ossim.domain.paging_in_process.LRU;
import miuoshki.ossim.domain.Process;

import java.util.*;
import java.util.stream.Collectors;

public abstract class FrameAllocationAlgorithm {

    protected int nOfFrames;
    protected int availableFrames;

    protected Map<Process, LRU> workingProcesses = new TreeMap<>();

    protected int globalPageErrorsSum = 0;
    protected int nOfSimulations = 0;

    protected double pageErrorsPerProcess = 0;

    public FrameAllocationAlgorithm(int nOfFrames) {
        this.nOfFrames = nOfFrames;
        this.availableFrames = nOfFrames;
    }

    /**
     * Accordingly to its class schema proceeds with simulation, counting global page errors sum
     *
     * @param processes list of processes with page requests
     * @return number of global errors
     */
    public int simulate(List<Process> processes) {

        int oldGlobalPageErrorsSum = globalPageErrorsSum;
        int nOfProcesses = processes.size();

        initializeMaps(processes);

        while (!workingProcesses.isEmpty()) {

            availableFrames = nOfFrames;

            workingProcesses = workingProcesses.entrySet().stream()
                    .sorted(
                            Map.Entry.comparingByKey(
                                    Comparator.comparingInt(
                                            process -> process.getProcessesIncomingPages().size()
                                    )
                            )
                    ).collect(
                            Collectors.toMap(
                                    Map.Entry::getKey,
                                    Map.Entry::getValue,
                                    (e1, e2) -> e1,
                                    LinkedHashMap::new
                            )
                    );

            for (Map.Entry<Process, LRU> simulation : workingProcesses.entrySet()) {

                if (!simulation.getKey().getProcessesIncomingPages().isEmpty()) {
                    int framesForProcess = calcFramesForProcess(simulation.getKey());
                    if (simulation.getKey().getMinFrames() <= framesForProcess) {
                        availableFrames -= framesForProcess;
                        simulation.getValue().simulateIteration(
                                simulation.getKey().getProcessesIncomingPages().remove(0),
                                framesForProcess
                        );
                    }
                }

            }

            List<Process> finished = workingProcesses.keySet().stream()
                    .filter(key -> key.getProcessesIncomingPages().isEmpty())
                    .collect(Collectors.toList());

            for (Process finishedProcess : finished) {
                finishedProcess.simulationFinished(workingProcesses.get(finishedProcess).getPageErrors());
                globalPageErrorsSum += workingProcesses.get(finishedProcess).getPageErrors();
                workingProcesses.remove(finishedProcess);
            }

        }

        nOfSimulations++;
        pageErrorsPerProcess += (globalPageErrorsSum - oldGlobalPageErrorsSum) / (double) nOfProcesses;

        return globalPageErrorsSum;
    }

    public int getAvgNumOfGlobalPageErrors() {
        return (nOfSimulations == 0) ? 0 : globalPageErrorsSum / nOfSimulations;
    }

    public double getAvgNumOfPageErrorsPerProcess() {
        return (nOfSimulations == 0) ? 0 : pageErrorsPerProcess / nOfSimulations;
    }

    /**
     * Given processes calculates how many frames should be allocated for him
     *
     * @param process for which we ought to calculate
     * @return number of frames allocated
     */
    protected abstract int calcFramesForProcess(Process process);

    protected void initializeMaps(List<Process> processes) {
        for (Process process : processes) {
            workingProcesses.put(process, new LRU());
        }
    }

}
