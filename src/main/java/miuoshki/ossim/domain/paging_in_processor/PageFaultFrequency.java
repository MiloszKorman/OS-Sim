package miuoshki.ossim.domain.paging_in_processor;

import miuoshki.ossim.domain.paging_in_process.LRU;
import miuoshki.ossim.domain.Process;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageFaultFrequency extends FrameAllocationAlgorithm {

    private static double LOWERBORDER = 0.25;
    private static double UPPERBORDER = 0.75;

    private Map<Process, Integer> allocatedFramesMap = new HashMap<>();

    public PageFaultFrequency(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    public int calcFramesForProcess(Process process) {

        int framesToAllocate = allocatedFramesMap.get(process);

        if (framesToAllocate <= availableFrames) {

            double pfRate = (UPPERBORDER + LOWERBORDER) / 2;
            if (0 < process.getSize() - process.getProcessesIncomingPages().size()) {
                pfRate =
                        workingProcesses.get(process).getPageErrors() /
                                (double) (process.getSize() - process.getProcessesIncomingPages().size());
            }

            if (pfRate <= LOWERBORDER && process.getMinFrames() < allocatedFramesMap.get(process)) {
                allocatedFramesMap.put(process, allocatedFramesMap.get(process) - 1);
            } else if (UPPERBORDER <= pfRate) {
                allocatedFramesMap.put(process, allocatedFramesMap.get(process) + 1);
            }

        }

        framesToAllocate = allocatedFramesMap.get(process);

        return framesToAllocate;
    }

    @Override
    protected void initializeMaps(List<Process> processes) {
        for (Process process : processes) {
            workingProcesses.put(process, new LRU());
            allocatedFramesMap.put(process, process.getMinFrames());
        }
    }

    public static void setLimits(double LOWERBORDER, double UPPERBORDER) {
        PageFaultFrequency.LOWERBORDER = LOWERBORDER;
        PageFaultFrequency.UPPERBORDER = UPPERBORDER;
    }

    @Override
    public String toString() {
        return "Page Fault Frequency";
    }
}
