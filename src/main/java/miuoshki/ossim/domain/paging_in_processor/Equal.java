package miuoshki.ossim.domain.paging_in_processor;

import miuoshki.ossim.domain.Process;

public class Equal extends FrameAllocationAlgorithm {

    public Equal(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    public int calcFramesForProcess(Process process) {

        int framesToAllocate = nOfFrames / workingProcesses.size();

        if (availableFrames < framesToAllocate) {
            framesToAllocate = availableFrames;
        }

        return (framesToAllocate < process.getMinFrames()) ? process.getMinFrames() : framesToAllocate;
    }

    @Override
    public String toString() {
        return "Equal";
    }
}
