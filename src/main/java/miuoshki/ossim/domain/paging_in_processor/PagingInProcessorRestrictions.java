package miuoshki.ossim.domain.paging_in_processor;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PagingInProcessorRestrictions {

    private int nOfProcesses;
    private int nOfFrames;
    private int maxPageRequests;

}
