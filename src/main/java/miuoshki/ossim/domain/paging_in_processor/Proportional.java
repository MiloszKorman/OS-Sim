package miuoshki.ossim.domain.paging_in_processor;

import miuoshki.ossim.domain.Process;

public class Proportional extends FrameAllocationAlgorithm {

    public Proportional(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    public int calcFramesForProcess(Process process) {
        int sizeSum = workingProcesses.keySet().stream()
                .map(processToSize -> processToSize.getProcessesIncomingPages().size())
                .reduce(0, (acc, processesSize) -> acc + processesSize);

        int framesToAllocate = nOfFrames * process.getProcessesIncomingPages().size() / sizeSum;

        if (availableFrames < framesToAllocate) {
            framesToAllocate = availableFrames;
        }

        return (framesToAllocate < process.getMinFrames()) ? process.getMinFrames() : framesToAllocate;
    }

    @Override
    public String toString() {
        return "Proportional";
    }
}
