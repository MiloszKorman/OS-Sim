package miuoshki.ossim.domain.paging_in_process;

import miuoshki.ossim.domain.Page;

import java.util.Comparator;

public class FIFO extends PageReplacementAlgorithm {

    public FIFO(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    protected Page nextToRemove() {
        return frames.stream()
                .min(Comparator.comparingInt(Page::getTimeOfArrival))
                .get();
    }

    @Override
    public String toString() {
        return "First In First Out";
    }
}
