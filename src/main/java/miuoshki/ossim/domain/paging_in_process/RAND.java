package miuoshki.ossim.domain.paging_in_process;

import miuoshki.ossim.domain.Page;

import java.util.SplittableRandom;

public class RAND extends PageReplacementAlgorithm {

    SplittableRandom random = new SplittableRandom();

    public RAND(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    protected Page nextToRemove() {
        return frames.get(random.nextInt(nOfFrames));
    }

    @Override
    public String toString() {
        return "Random";
    }
}
