package miuoshki.ossim.domain.paging_in_process;

import lombok.NoArgsConstructor;
import miuoshki.ossim.domain.Page;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public abstract class PageReplacementAlgorithm {

    protected int nOfFrames;
    protected List<Page> frames = new ArrayList<>();
    protected List<Page> listOfPages;
    protected int pageErrors = 0;
    protected int clock = 0;

    protected int sumOfPageErrors = 0;
    protected int nOfSimulations = 0;

    public PageReplacementAlgorithm(int nOfFrames) {
        this.nOfFrames = nOfFrames;
        frames = new ArrayList<>(nOfFrames);
    }

    /**
     * Simulates virtualization of memory on provided list of incoming pages, according to classes scheme
     *
     * @param incomingPages list of incoming pages
     * @return number of page errors
     */
    public int simulate(List<Page> incomingPages) {

        pageErrors = 0;
        clock = 0;
        this.listOfPages = incomingPages;
        frames = new ArrayList<>(nOfFrames);

        while (!incomingPages.isEmpty()) {

            Page currentPage = incomingPages.remove(0);

            if (frames.contains(currentPage)) {
                frames.get(frames.indexOf(currentPage)).setTimeOfLastUsage(clock);
            } else {
                currentPage.setTimeOfArrival(clock);

                if (frames.size() == nOfFrames) {
                    frames.remove(
                            nextToRemove()
                    );
                }

                frames.add(currentPage);

                pageErrors++;
            }

            clock++;

        }

        sumOfPageErrors += pageErrors;
        nOfSimulations++;
        return pageErrors;
    }

    /**
     * When free space in frames is needed, this method finds next page from frames to remove, according to classes
     * scheme
     *
     * @return Page to remove from frames
     */
    protected abstract Page nextToRemove();

    /**
     * @return number of frames allocated for this simulation
     */
    public int getnOfFrames() {
        return nOfFrames;
    }

    /**
     * Calculates how many page errors occurred on average per simulation
     *
     * @return number of average page errors
     */
    public int getAveragePageErrors() {
        return (nOfSimulations == 0) ? 0 : sumOfPageErrors / nOfSimulations;
    }

    //for so4
    public void simulateIteration(Page currentPage, int allocatedFrames) {

        if (frames.contains(currentPage)) {
            frames.get(frames.indexOf(currentPage)).setTimeOfLastUsage(clock);
        }

        while (allocatedFrames < frames.size()) {
            frames.remove(nextToRemove());
        }

        if (!frames.contains(currentPage)) {
            currentPage.setTimeOfArrival(clock);

            if (frames.size() == allocatedFrames) {
                frames.remove(nextToRemove());
            }

            frames.add(currentPage);

            pageErrors++;
        }

        clock++;
    }

    //for so4
    public int getPageErrors() {
        return pageErrors;
    }

}
