package miuoshki.ossim.domain.paging_in_process;

import miuoshki.ossim.domain.Page;

public class OPT extends PageReplacementAlgorithm {

    public OPT(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    protected Page nextToRemove() {

        Page longestNotUsed = null;
        int indexOfLNU = 0;

        for (Page page : frames) {
            if (!listOfPages.contains(page)) {
                return page;
            }
            if (indexOfLNU < listOfPages.indexOf(page)) {
                longestNotUsed = page;
                indexOfLNU = listOfPages.indexOf(page);
            }
        }

        return longestNotUsed;
    }

    @Override
    public String toString() {
        return "Optimal";
    }
}
