package miuoshki.ossim.domain.paging_in_process;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PagingInProcessRestrictions {

    private int nOfFrames;
    private int nOfPages;
    private int nOfIncomingPages;

}
