package miuoshki.ossim.domain.paging_in_process;

import miuoshki.ossim.domain.Page;

import java.util.Comparator;

public class ALRU extends PageReplacementAlgorithm {

    public ALRU(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    protected Page nextToRemove() {

        frames.sort(Comparator.comparing(Page::getTimeOfLastUsage));

        Page toReturn = null;

        while (toReturn == null) {

            if (frames.get(0).isSecondChance()) {
                frames.get(0).setSecondChance(false);
                frames.add(frames.remove(0));
            } else {
                toReturn = frames.get(0);
            }

        }

        return toReturn;
    }

    @Override
    public String toString() {
        return "Approximately Least recently used";
    }
}
