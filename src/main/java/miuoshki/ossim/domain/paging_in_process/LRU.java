package miuoshki.ossim.domain.paging_in_process;

import lombok.NoArgsConstructor;
import miuoshki.ossim.domain.Page;

import java.util.Comparator;

@NoArgsConstructor
public class LRU extends PageReplacementAlgorithm {

    public LRU(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    protected Page nextToRemove() {
        return frames.stream()
                .min(Comparator.comparingInt(Page::getTimeOfLastUsage))
                .get();
    }

    @Override
    public String toString() {
        return "Least Recently Used";
    }
}
