package miuoshki.ossim.domain.distributed_process_scheduling;

import miuoshki.ossim.domain.Processor;
import miuoshki.ossim.domain.Process;

import java.util.List;
import java.util.Set;
import java.util.SplittableRandom;

public class XLast extends ProcessorAllocationAlgorithm {

    SplittableRandom random = new SplittableRandom();

    public XLast(int MAX_N_OF_INQUIRIES, int UPPER_BOUND, int LOWER_BOUND) {
        super(MAX_N_OF_INQUIRIES, UPPER_BOUND, LOWER_BOUND);
    }

    @Override
    protected void allocateProcesses(Set<Processor> workingProcessors, List<Process> toRemove,
                                     List<Processor> possibleExecutors, Process process) {

        Processor foreignProcessor = pickForeignProcessor(possibleExecutors, process);
        if (foreignProcessor == null) {
            if (process.getOrigin().getCurrentUsage() + process.getCpuUsageLevel() <= 100) {
                process.getOrigin().executeProcess(process);
                toRemove.add(process);
            }
        } else {
            executeOnForeignProcessor(foreignProcessor, process, toRemove);
        }

    }

    @Override
    public String toString() {
        return "X Last";
    }
}
