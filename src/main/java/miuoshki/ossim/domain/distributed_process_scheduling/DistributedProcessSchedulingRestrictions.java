package miuoshki.ossim.domain.distributed_process_scheduling;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DistributedProcessSchedulingRestrictions {

    private int nOfProcessors;
    private int maxNOfProcessesPerProcessor;
    private int maxNOfInquiries;
    private int lowerBoundary;
    private int upperBoundary;

}
