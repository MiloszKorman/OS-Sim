package miuoshki.ossim.domain.distributed_process_scheduling;

import miuoshki.ossim.domain.Processor;
import miuoshki.ossim.domain.Process;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class XCaring extends XFirst {

    public XCaring(int MAX_N_OF_INQUIRIES, int UPPER_BOUND, int LOWER_BOUND) {
        super(MAX_N_OF_INQUIRIES, UPPER_BOUND, LOWER_BOUND);
    }

    @Override
    protected void allocateProcesses(Set<Processor> workingProcessors, List<Process> toRemove,
                                     List<Processor> possibleExecutors, Process process) {

        super.allocateProcesses(workingProcessors, toRemove, possibleExecutors, process);

        for (Processor unoccupiedProcessor : workingProcessors) {
            if (unoccupiedProcessor.getCurrentUsage() <= LOWER_BOUND) {
                List<Processor> occupiedProcessors = workingProcessors.stream()
                        .filter(processor -> UPPER_BOUND <= processor.getCurrentUsage())
                        .collect(Collectors.toList());

                for (int i = 0;
                     i < MAX_N_OF_INQUIRIES &&
                             !occupiedProcessors.isEmpty() &&
                             unoccupiedProcessor.getCurrentUsage() <= UPPER_BOUND;
                     i++) {

                    nOfInquiries++;

                    Processor occupiedProcessor = occupiedProcessors.get(
                            random.nextInt(0, occupiedProcessors.size())
                    );

                    occupiedProcessors.remove(occupiedProcessor);

                    while (UPPER_BOUND <= occupiedProcessor.getCurrentUsage()) {
                        nOfMigrations++;
                        Process processToTransport = occupiedProcessor.giveBackProcess();
                        unoccupiedProcessor.executeProcess(processToTransport);
                    }

                }
            }
        }

    }

    @Override
    public String toString() {
        return "X Caring";
    }
}
