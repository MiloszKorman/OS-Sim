package miuoshki.ossim.domain.distributed_process_scheduling;

import miuoshki.ossim.domain.Processor;
import miuoshki.ossim.domain.Process;

import java.util.List;
import java.util.Set;

public class XFirst extends XLast {

    public XFirst(int MAX_N_OF_INQUIRIES, int UPPER_BOUND, int LOWER_BOUND) {
        super(MAX_N_OF_INQUIRIES, UPPER_BOUND, LOWER_BOUND);
    }

    @Override
    protected void allocateProcesses(Set<Processor> workingProcessors, List<Process> toRemove,
                                     List<Processor> possibleExecutors, Process process) {

        if (canProcessorExecute(process.getOrigin(), process)) {
            process.getOrigin().executeProcess(process);
            toRemove.add(process);
        } else {
            super.allocateProcesses(workingProcessors, toRemove, possibleExecutors, process);
        }

    }

    @Override
    public String toString() {
        return "X First";
    }
}
