package miuoshki.ossim.domain.distributed_process_scheduling;

import java.util.List;
import java.util.Map;

public class Stats {

    private double avgProcessorsUsage = 0;
    private double avgDeviations = 0;
    private int nOfInquiriesSum = 0;
    private int nOfMigrationsSum = 0;

    private int nOfSimulations = 0;

    private double calcAverageProcessorUsage(Map<Integer, List<Integer>> historyOfUsage) {

        for (List<Integer> iterationsHistory : historyOfUsage.values()) {
            avgProcessorsUsage += iterationsHistory.stream().reduce(0, (acc, cpuUsage) -> acc + cpuUsage)
                    / (double)iterationsHistory.size();
        }

        avgProcessorsUsage /= historyOfUsage.size();
        return avgProcessorsUsage;
    }

    private double calcAverageDeviation(Map<Integer, List<Integer>> historyOfUsage, double avgCpuUsage) {

        for (List<Integer> iterationsHistory : historyOfUsage.values()) {
            double iterationsDiviationSum = 0;
            for (int cpuUsage : iterationsHistory) {
                iterationsDiviationSum += Math.abs(avgCpuUsage - cpuUsage);
            }
            avgDeviations += iterationsDiviationSum/iterationsHistory.size();
        }

        avgDeviations /= historyOfUsage.size();
        return avgDeviations;
    }

    public double getAvgProcessorsUsage() {
        return (nOfSimulations == 0) ? 0 : avgProcessorsUsage /nOfSimulations;
    }

    public double getAvgDeviation() {
        return (nOfSimulations == 0) ? 0 : avgDeviations /nOfSimulations;
    }

    public int getNumberOfInteractions() {
        return (nOfSimulations == 0) ? 0 : (nOfInquiriesSum+nOfMigrationsSum)/nOfSimulations;
    }

    public int getNumberOfInquiries() {
        return (nOfSimulations == 0) ? 0 : nOfInquiriesSum/nOfSimulations;
    }

    public int getNumberOfMigrations() {
        return (nOfSimulations == 0) ? 0 : nOfMigrationsSum/nOfSimulations;
    }

    public void addAll(Map<Integer, List<Integer>> historyOfUsage, int nOfInquiries, int nOfMigrations) {
        calcAverageDeviation(historyOfUsage, calcAverageProcessorUsage(historyOfUsage));
        this.nOfInquiriesSum += nOfInquiries;
        this.nOfMigrationsSum += nOfMigrations;
        nOfSimulations++;
    }

}
