package miuoshki.ossim.domain.distributed_process_scheduling;

import miuoshki.ossim.domain.Process;
import miuoshki.ossim.domain.Processor;

import java.util.*;
import java.util.stream.Collectors;

public abstract class ProcessorAllocationAlgorithm {

    private SplittableRandom random = new SplittableRandom();

    protected final int MAX_N_OF_INQUIRIES;
    protected final int UPPER_BOUND;
    protected final int LOWER_BOUND;

    protected int clock;

    protected int nOfInquiries;
    protected int nOfMigrations;

    protected Map<Integer, List<Integer>> history;
    protected List<Process> processesToAllocate;
    protected final Stats myStats;

    public ProcessorAllocationAlgorithm(int MAX_N_OF_INQUIRIES, int UPPER_BOUND, int LOWER_BOUND) {
        this.MAX_N_OF_INQUIRIES = MAX_N_OF_INQUIRIES;
        this.UPPER_BOUND = UPPER_BOUND;
        this.LOWER_BOUND = LOWER_BOUND;
        myStats = new Stats();
    }

    protected void reset() {
        clock = 0;
        nOfInquiries = 0;
        nOfMigrations = 0;
        history = new HashMap<>();
        processesToAllocate = new ArrayList<>();
    }

    /**
     * Simulates algorithms handling of processors scheduling for incoming processes accordingly to classes schema
     *
     * @param workingProcessors all processors working in the system with incoming processes
     */
    public void simulate(Set<Processor> workingProcessors) {
        reset();

        boolean end;

        do {

            /*if (!processesToAllocate.isEmpty()) {
                System.out.println("Iteration " + clock + ", unallocated processes: " + processesToAllocate.size());
            }*/

            processesToAllocate.addAll(
                    workingProcessors.stream()
                            .flatMap(processor -> processor.getWaitingProcesses().stream())
                            .filter(process -> process.getTimeOfArrival() <= clock)
                            .collect(Collectors.toList())
            );

            processesToAllocate.forEach(
                    process -> process.getOrigin().getWaitingProcesses().remove(process)
            );

            List<Process> toRemove = new LinkedList<>();

            for (Process process : processesToAllocate) {
                List<Processor> possibleExecutors = workingProcessors.stream()
                        .filter(processor -> processor != process.getOrigin())
                        .collect(Collectors.toList());

                allocateProcesses(workingProcessors, toRemove, possibleExecutors, process);
            }

            processesToAllocate.removeAll(toRemove);

            workingProcessors.forEach(Processor::iteration);

            clock++;

            history.put(
                    clock,
                    workingProcessors.stream()
                            .map(Processor::getCurrentUsage)
                            .collect(Collectors.toList())
            );

            end = workingProcessors.stream()
                    .allMatch(processor ->
                            processor.getWaitingProcesses().isEmpty() && processor.getWorkingProcesses().isEmpty()
                    );

            end = end && processesToAllocate.isEmpty();

        } while (!end);

        myStats.addAll(history, nOfInquiries, nOfMigrations);
    }

    public Stats getStats() {
        return myStats;
    }

    /**
     * Each class implements how it's done. Available process is supposed to be assigned to available processors
     *
     * @param workingProcessors list of available processors in the system
     * @param toRemove collector of assigned processes
     * @param possibleExecutors processors that aren't overloaded and aren't processes origin
     * @param process that is ought to be allocated
     */
    protected abstract void allocateProcesses(Set<Processor> workingProcessors, List<Process> toRemove, List<Processor> possibleExecutors, Process process);

    /**
     * Checks whether this processor is able to execute this processes without getting overloaded
     *
     * @param processor that could be executing process
     * @param process process ought to be allocated
     * @return boolean value whether this processor can execute this process
     */
    protected boolean canProcessorExecute(Processor processor, Process process) {
        return (processor != null &&
                processor.getCurrentUsage() <= UPPER_BOUND &&
                processor.getCurrentUsage() + process.getCpuUsageLevel() <= 100);
    }

    /**
     * Adds process to processors workload, toRemove collector and increments number of migrations
     *
     * @param processor executor of the process
     * @param process process that has been allocated
     * @param toRemove collector of assigned processes
     */
    protected void executeOnForeignProcessor(Processor processor, Process process, List<Process> toRemove) {
        processor.executeProcess(process);
        nOfMigrations++;
        toRemove.add(process);
    }

    /**
     * Checks within legal number of inquiries whether there is available processor to execute this process
     *
     * @param possibleExecutors list of processors that are not processes origin
     * @param process that is supposed to be allocated
     * @return processor if one has been picked, otherwise null
     */
    protected Processor pickForeignProcessor(List<Processor> possibleExecutors, Process process) {

        for (int i = 0; i < MAX_N_OF_INQUIRIES && !possibleExecutors.isEmpty(); i++) {

            nOfInquiries++;

            Processor randomProcessor;

            randomProcessor = possibleExecutors.get(random.nextInt(0, possibleExecutors.size()));

            if (canProcessorExecute(randomProcessor, process)) {
                return randomProcessor;
            }

            possibleExecutors.remove(randomProcessor);

        }

        return null;
    }

}
