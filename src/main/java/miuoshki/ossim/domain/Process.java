package miuoshki.ossim.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

@Getter
@Setter
public class Process implements Comparable {

    private static int numberOfProcesses = 0;

    private int size;
    private int minFrames;
    List<Page> processesIncomingPages;

    int nOfSimulations = 0;
    int pageErrorsSum = 0;

    int allocatedFrames;

    private int id;
    private int timeOfCpuUsage;
    private int remainingTimeOfCpuUsage;
    private int timeOfArrival = 0;
    private int timeWaiting = 0;

    private int cpuUsageLevel;
    private Processor origin = null;

    //assigns id for newly created processes, based on how many have been created
    //up till this point
    private static int assignId() {
        return ++numberOfProcesses;
    }

    public Process(int timeOfCpuUsage, int timeOfArrival) {
        id = assignId();
        this.timeOfCpuUsage = timeOfCpuUsage;
        this.remainingTimeOfCpuUsage = timeOfCpuUsage;
        this.timeOfArrival = timeOfArrival;
    }

    public Process(int minFrames) {
        this.minFrames = minFrames;
        this.allocatedFrames = minFrames;
    }

    public Process(List<Page> processesIncomingPages, int minFrames) {
        this(minFrames);
        setProcessesIncomingPages(processesIncomingPages);
    }

    public Process(int timeOfArrival, int cpuUsageLevel, int timeOfCpuUsage) {
        this.timeOfArrival = timeOfArrival;
        this.cpuUsageLevel = cpuUsageLevel;
        this.timeOfCpuUsage = timeOfCpuUsage;
    }

    public void setProcessesIncomingPages(List<Page> processesIncomingPages) {
        for (Page page : processesIncomingPages) {
            page.setOwner(this);
        }
        this.processesIncomingPages = processesIncomingPages;
        size = processesIncomingPages.size();
    }

    public void simulationFinished(int pageErrors) {
        pageErrorsSum += pageErrors;
        nOfSimulations++;
    }

    public void reset() {
        this.remainingTimeOfCpuUsage = timeOfCpuUsage;
    }

    //compares processes by which came first
    public static int compareByArrival(Process process1, Process process2) {
        return Integer.compare(process1.timeOfArrival, process2.timeOfArrival);
    }

    //compares processes by which is to finish first
    public static int compareByRemainingCpuTime(Process process1, Process process2) {
        return Integer.compare(process1.remainingTimeOfCpuUsage, process2.remainingTimeOfCpuUsage);
    }

    public Process cloneProcess() {
        Process processToReturn = new Process(minFrames);
        processToReturn.setProcessesIncomingPages(new LinkedList<>(processesIncomingPages));
        return processToReturn;
    }

    public void iteration() {
        if (0 < timeOfCpuUsage) {
            timeOfCpuUsage--;
        }
    }

    public Process clone() {
        return new Process(timeOfArrival, cpuUsageLevel, timeOfCpuUsage);
    }

    @Override
    public int compareTo(Object o) {
        return Integer.compare(this.size, ((Process)o).getSize());
    }

}
