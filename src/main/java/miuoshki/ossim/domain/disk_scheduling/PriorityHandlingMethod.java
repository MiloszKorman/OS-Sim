package miuoshki.ossim.domain.disk_scheduling;

public enum PriorityHandlingMethod {
    EDF, FDSCAN, NONE
}
