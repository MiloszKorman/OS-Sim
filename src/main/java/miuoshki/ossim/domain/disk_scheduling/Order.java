package miuoshki.ossim.domain.disk_scheduling;

import lombok.Getter;

@Getter
public class Order {

    private int sectorToRead;
    private int arrival;
    private int deadline = -1;

    public Order(int sectorToRead, int arrival) {
        this.sectorToRead = sectorToRead;
        this.arrival = arrival;
    }

    public Order(int sectorToRead, int arrival, int deadline) {
        this(sectorToRead, arrival);
        this.deadline = arrival + deadline;
    }

    public static int compareByArrival(Order o1, Order o2) {
        return Integer.compare(o1.arrival, o2.arrival);
    }

    public static int compareByDeadline(Order o1, Order o2) {
        return Integer.compare(o1.deadline, o2.deadline);
    }

}
