package miuoshki.ossim.domain.disk_scheduling.algorithms;

import miuoshki.ossim.domain.disk_scheduling.Order;
import miuoshki.ossim.domain.disk_scheduling.PriorityHandlingMethod;

public class SSTF extends PlanningAlgorithm {

    public SSTF(int startingSector) {
        super(startingSector);
    }

    public SSTF(int startingSector, PriorityHandlingMethod priorityHandling) {
        super(startingSector, priorityHandling);
    }

    @Override
    protected Order getNextRegularOrder() {
        return findNearest(availableOrders);
    }

    @Override
    public String toString() {
        return "Shortest Seek First";
    }

}
