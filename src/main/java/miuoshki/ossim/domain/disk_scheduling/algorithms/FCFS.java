package miuoshki.ossim.domain.disk_scheduling.algorithms;

import miuoshki.ossim.domain.disk_scheduling.Order;
import miuoshki.ossim.domain.disk_scheduling.PriorityHandlingMethod;

public class FCFS extends PlanningAlgorithm {

    public FCFS(int startingSector) {
        super(startingSector);
    }

    public FCFS(int startingSector, PriorityHandlingMethod priorityHandling) {
        super(startingSector, priorityHandling);
    }

    @Override
    protected Order getNextRegularOrder() {
        if (availableOrders.isEmpty()) {
            return null;
        } else {
            return availableOrders.get(0);
        }
    }

    @Override
    public String toString() {
        return "First Come, First Served";
    }
}
