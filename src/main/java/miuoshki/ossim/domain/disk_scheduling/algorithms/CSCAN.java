package miuoshki.ossim.domain.disk_scheduling.algorithms;

import miuoshki.ossim.domain.disk_scheduling.Order;
import miuoshki.ossim.domain.disk_scheduling.PriorityHandlingMethod;

import java.util.ArrayList;

public class CSCAN extends MovingPlanningAlgorithm {

    public CSCAN(int startingSector) {
        super(startingSector);
    }

    public CSCAN(int startingSector, PriorityHandlingMethod priorityHandling) {
        super(startingSector, priorityHandling);
    }

    @Override
    public int simulate(ArrayList<Order> ordersQueue) {
        return super.simulate(ordersQueue);
    }

    @Override
    protected Order availableOrdersInWrongDirection() {
        int timeComing;
        timeComing = isOrderComingWithinThisCycle(MAX_SECTOR);
        if (timeComing == -1) {
            moveTo(timeWorking + ((MAX_SECTOR - currentSector) * MOVE_TIME), new Order(MAX_SECTOR, 0));
            cscanGoingLeft = true;
            return new Order(1, 0);
        } else {
            return new Order(MAX_SECTOR, 0);
        }
    }

    @Override
    public String toString() {
        return "Circular Elevator";
    }

}
