package miuoshki.ossim.domain.disk_scheduling.algorithms;

import miuoshki.ossim.domain.disk_scheduling.Direction;
import miuoshki.ossim.domain.disk_scheduling.Order;
import miuoshki.ossim.domain.disk_scheduling.PriorityHandlingMethod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class PlanningAlgorithm {

    //for HDD with capacity of 512 kB and sectors size 512 B
    protected static final int MAX_SECTOR = 1000;
    //time in which head passes one sector
    protected static final int MOVE_TIME = 1;
    //sector on which head currently is, initially random legal value
    protected int currentSector;
    int myStartingSector;
    //always starts right, later may change
    protected Direction currentDirection = Direction.RIGHT;

    protected int timeWorking = 0;
    protected int prevTimeWorking = -1;
    protected int donePastDeadline = 0;
    protected int headMovesSum = 0;

    protected int simulationsDone = 0;
    protected long allSimulationsHeadMoveSum = 0;
    protected long allMissedDeadlines = 0;

    protected ArrayList<Order> allOrders;
    protected ArrayList<Order> availableOrders = new ArrayList<>();
    protected ArrayList<Order> priorityOrders = new ArrayList<>();
    protected ArrayList<Order> availablePriorityOrders = new ArrayList<>();

    protected PriorityHandlingMethod priorityHandling = PriorityHandlingMethod.NONE;

    public PlanningAlgorithm(int startingSector) {
        currentSector = startingSector;
        myStartingSector = startingSector;
    }

    public PlanningAlgorithm(int startingSector, PriorityHandlingMethod priorityHandling) {
        this(startingSector);
        this.priorityHandling = priorityHandling;
    }

    /**
     * This method, given queue of orders, simulates disks head movements,
     * accordingly to instance of the class methodology
     *
     * @param ordersQueue accepts queue of orders on which simulation is ought to be carried out
     * @return returns sum of all head moves
     **/
    public int simulate(ArrayList<Order> ordersQueue) {

        reset();

        Collections.sort(ordersQueue, Order::compareByArrival);
        allOrders = ordersQueue;

        initalizePriorityQueue();

        while (!allOrders.isEmpty()) {

            updateAvailableQueues();

            Order orderToFinish = getNextOrder();

            if (orderToFinish == null) {
                timeWorking = allOrders.get(0).getArrival();
            } else {
                int timeComing = isOrderComingWithinThisCycle(orderToFinish.getSectorToRead());

                if (timeComing == -1) {
                    finishOrder(orderToFinish);
                } else {
                    moveTo(timeComing, orderToFinish);
                }
            }

        }

        allSimulationsHeadMoveSum += headMovesSum;
        allMissedDeadlines += donePastDeadline;
        simulationsDone++;
        return headMovesSum;
    }

    public int howManyPastDeadline() {
        return donePastDeadline;
    }

    /**
     * This method, given queue in which is expected to search, finds and returns nearest element to
     * current heads position
     *
     * @param queue queue in which nearest element is ought to be found
     * @return order that is nearest to current heads position
     */
    protected Order findNearest(List<Order> queue) {
        if (queue.isEmpty()) {
            return null;
        } else {
            return queue.stream()
                    .min(Comparator.comparing(order -> Math.abs(order.getSectorToRead() - currentSector)))
                    .get();
        }
    }

    /**
     * This method, adds all orders meting certain requirements to appropriate queues
     */
    protected void updateAvailableQueues() {
        availableOrders.clear();
        availableOrders.addAll(
                allOrders.stream()
                        .filter(order -> order.getDeadline() == -1 && order.getArrival() <= timeWorking)
                        .collect(Collectors.toList())
        );
        availablePriorityOrders.clear();
        availablePriorityOrders.addAll(
                priorityOrders.stream()
                        .filter(order -> order.getArrival() <= timeWorking)
                        .collect(Collectors.toList())
        );
    }

    /**
     * Copies all priority orders from all orders queue to priority orders queue
     */
    protected void initalizePriorityQueue() {
        if (!allOrders.isEmpty()) {
            priorityOrders.addAll(
                    allOrders.stream()
                            .filter(order -> 0 < order.getDeadline())
                            .collect(Collectors.toList())
            );
        }
        if (priorityHandling == PriorityHandlingMethod.NONE) {
            allOrders.removeAll(priorityOrders);
        }
    }

    /**
     * Based on both available both priority and regular queues picks an order which is ought to be finished next
     *
     * @return order which is ought to be finished
     */
    protected Order getNextOrder() {
        Order toReturn = null;

        if (!availablePriorityOrders.isEmpty()) {
            if (priorityHandling == PriorityHandlingMethod.EDF) {
                toReturn = availablePriorityOrders.stream().min(Comparator.comparing(Order::getDeadline)).get();
            } else if (priorityHandling == PriorityHandlingMethod.FDSCAN) {
                List<Order> correctSide = availablePriorityOrders
                        .stream().filter(order ->
                                MovingPlanningAlgorithm.isOrderOnCorrectSide(currentDirection, currentSector, order))
                        .collect(Collectors.toList());
                if (correctSide.isEmpty()) {
                    if (currentDirection == Direction.RIGHT) {
                        currentDirection = Direction.LEFT;
                    } else {
                        currentDirection = Direction.RIGHT;
                    }
                    return new Order(currentSector, 0);
                } else {
                    return findNearest(correctSide);
                }
            }
        }

        if (toReturn == null) {
            toReturn = getNextRegularOrder();
        }

        return toReturn;
    }

    /**
     * Based on available regular orders queue picks an order which is ought to be finished next
     *
     * @return order which is ought to be finished
     */
    protected abstract Order getNextRegularOrder();

    /**
     * This method, given order, executes and removes it.
     * Increments sum of head moves and time working. Updates current sector on which the head is.
     * If it was a priority order, and hasn't been finished within its deadline, donePastDeadline is incremented.
     *
     * @param currentOrder order that's ought to be executed and removed
     */
    protected void finishOrder(Order currentOrder) {

        ArrayList<Order> ordersToErase = new ArrayList<>();

        if (priorityHandling == PriorityHandlingMethod.FDSCAN) {
            if (0 < currentOrder.getDeadline()) {
                for (Order order : availableOrders) {
                    if (order != currentOrder) {
                        if (currentSector < order.getSectorToRead() && order.getSectorToRead() <= currentOrder.getSectorToRead()) {
                            ordersToErase.add(order);
                        } else if (currentOrder.getSectorToRead() <= order.getSectorToRead() && order.getSectorToRead() < currentSector) {
                            ordersToErase.add(order);
                        }
                    }
                }
            }
        }

        //Remove requests for same disc space
        for (Order order : allOrders) {
            if (order.getArrival() <= timeWorking + (Math.abs(currentOrder.getSectorToRead() - currentSector) * MOVE_TIME)) {
                if (order.getSectorToRead() == currentOrder.getSectorToRead()) {
                    ordersToErase.add(order);
                }
            }
        }

        for (Order order : ordersToErase) {
            eraseOrder(order);
        }

        headMovesSum += Math.abs(currentOrder.getSectorToRead() - currentSector);
        prevTimeWorking = timeWorking;
        timeWorking += (Math.abs(currentOrder.getSectorToRead() - currentSector) * MOVE_TIME);

        if (0 < currentOrder.getDeadline() && currentOrder.getDeadline() < timeWorking) {
            donePastDeadline++;
        }

        currentSector = currentOrder.getSectorToRead();
        eraseOrder(currentOrder);
    }

    /**
     * Restores algorithms properties to initial values
     */
    protected void reset() {
        currentDirection = Direction.RIGHT;
        timeWorking = 0;
        prevTimeWorking = -1;
        headMovesSum = 0;
        allOrders = new ArrayList<>();
        availableOrders = new ArrayList<>();
        donePastDeadline = 0;
        currentSector = myStartingSector;
    }

    /**
     * Checks whether another order is coming within this move between current sector and provided one
     *
     * @param goingToSector sector where are we going
     * @return returns time when new order is coming, if none is, then returns -1
     */
    protected int isOrderComingWithinThisCycle(int goingToSector) {

        for (Order order : allOrders) {
            if (timeWorking < order.getArrival() && order.getArrival() < timeWorking + (Math.abs(currentSector - goingToSector) * MOVE_TIME)) {
                return order.getArrival();
            }
        }

        return -1;
    }

    /**
     * Moves to provided time, in direction of current order
     *
     * @param time         at which time to move
     * @param currentOrder order to which we were about execute, we go in it's direction
     */
    protected void moveTo(int time, Order currentOrder) {

        headMovesSum += (time - timeWorking) / MOVE_TIME;
        if (currentSector < currentOrder.getSectorToRead()) {
            currentSector += (time - timeWorking) / MOVE_TIME;
        } else {
            currentSector -= (time - timeWorking) / MOVE_TIME;
        }
        prevTimeWorking = timeWorking;
        timeWorking = time;

    }

    public static int getMaxSector() {
        return MAX_SECTOR;
    }

    public int getAverageHeadMoves() {
        if (0 < simulationsDone) {
            return (int) (allSimulationsHeadMoveSum / simulationsDone);
        } else {
            return -1;
        }
    }

    public int getAverageMissedDeadlines() {
        if (0 < simulationsDone) {
            return (int) (allMissedDeadlines / simulationsDone);
        } else {
            return -1;
        }
    }

    /**
     * Erases provided order from queues it's in
     *
     * @param order order to erase
     */
    protected void eraseOrder(Order order) {
        allOrders.remove(order);
        availableOrders.remove(order);
        priorityOrders.remove(order);
        availablePriorityOrders.remove(order);
    }

    /**
     * Checks whether provided order is on correct side from current sector
     *
     * @return whether it's on correct side or not
     */
    protected static boolean isOrderOnCorrectSide(Direction currentDirection, int currentSector, Order toGo) {
        if (currentDirection == Direction.RIGHT && currentSector <= toGo.getSectorToRead()) {
            return true;
        } else if (currentDirection == Direction.LEFT && toGo.getSectorToRead() <= currentSector) {
            return true;
        } else {
            return false;
        }
    }

    public PriorityHandlingMethod getPriorityHandling() {
        return priorityHandling;
    }
}
