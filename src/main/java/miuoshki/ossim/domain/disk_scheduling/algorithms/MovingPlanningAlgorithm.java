package miuoshki.ossim.domain.disk_scheduling.algorithms;

import miuoshki.ossim.domain.disk_scheduling.Direction;
import miuoshki.ossim.domain.disk_scheduling.Order;
import miuoshki.ossim.domain.disk_scheduling.PriorityHandlingMethod;

import java.util.ArrayList;
import java.util.stream.Collectors;

public abstract class MovingPlanningAlgorithm extends PlanningAlgorithm {

    protected ArrayList<Order> ordersInCorrectDirection = new ArrayList<>();

    public MovingPlanningAlgorithm(int startingSector) {
        super(startingSector);
    }

    public MovingPlanningAlgorithm(int startingSector, PriorityHandlingMethod priorityHandling) {
        super(startingSector, priorityHandling);
    }

    protected boolean cscanGoingLeft = false;

    /**
     * This method, checks in which direction head is going, then adds only orders on this side, from all available
     * orders queue, to orders on correct side queue
     */
    protected void addOrdersOnCorrectSide() {
        ordersInCorrectDirection.clear();
        ordersInCorrectDirection.addAll(
                availableOrders.stream()
                        .filter(order -> isOrderOnCorrectSide(currentDirection, currentSector, order))
                        .collect(Collectors.toList())
        );
    }

    @Override
    protected void updateAvailableQueues() {
        super.updateAvailableQueues();
        addOrdersOnCorrectSide();
    }

    @Override
    protected void reset() {
        super.reset();
        ordersInCorrectDirection = new ArrayList<>();
    }

    @Override
    protected void eraseOrder(Order order) {
        super.eraseOrder(order);
        ordersInCorrectDirection.remove(order);
    }

    @Override
    protected Order getNextRegularOrder() {

        if (this.getClass() == CSCAN.class) {
            if (currentDirection == Direction.LEFT) {
                currentDirection = Direction.RIGHT;
                updateAvailableQueues();
            }

            if (cscanGoingLeft && currentSector != 1) {
                return new Order(1, 0);
            }
        }

        cscanGoingLeft = false;

        if (availableOrders.isEmpty()) {
            return null;
        } else {
            if (ordersInCorrectDirection.isEmpty()) {
                return availableOrdersInWrongDirection();
            } else {
                return findNearest(ordersInCorrectDirection);
            }
        }
    }

    /**
     * Performs necessary operations in case of a situation in which orders are available, but on the opposite
     * side
     *
     * @return order in which direction we will go
     */
    protected abstract Order availableOrdersInWrongDirection();

}
