package miuoshki.ossim.domain.disk_scheduling;

public enum Direction {
    LEFT, RIGHT
}
