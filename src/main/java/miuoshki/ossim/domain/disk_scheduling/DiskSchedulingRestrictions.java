package miuoshki.ossim.domain.disk_scheduling;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DiskSchedulingRestrictions {

    private int startingSector;
    private int queueSize;
    private int howLate;
    private int nOfDeadliners;
    private int maxDeadline;
    private int minDeadline;

}
