package miuoshki.ossim.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Page {

    private int number;
    private int timeOfArrival = Integer.MAX_VALUE;
    private int timeOfLastUsage = Integer.MAX_VALUE;
    private boolean secondChance = true;

    private Process owner;

    public Page(int number) {
        this.number = number;
    }

    public void setTimeOfArrival(int timeOfArrival) {
        this.timeOfArrival = timeOfArrival;
        this.timeOfLastUsage = timeOfArrival;
    }

    public boolean isSecondChance() {
        return secondChance;
    }

    public void setSecondChance(boolean secondChance) {
        this.secondChance = secondChance;
    }

    @Override
    public boolean equals(Object obj) {
        return (this.number == ((Page)obj).number);
    }

}
