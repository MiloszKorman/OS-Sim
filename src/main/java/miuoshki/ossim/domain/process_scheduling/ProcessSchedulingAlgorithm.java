package miuoshki.ossim.domain.process_scheduling;

import miuoshki.ossim.domain.Process;

import java.util.List;

public abstract class ProcessSchedulingAlgorithm {

    private long waitingTimeSum = 0;
    private int numberOfProcessesFinished = 0;

    protected int simulationsSum = 0;
    protected int nOfSimulations = 0;

    //is calculated by dividing sum of all processes waiting times and number of processes finished
    protected double getAverageWaitingTime() {
        if (numberOfProcessesFinished == 0) {
            return -1;
        } else {
            return ((double) waitingTimeSum) / numberOfProcessesFinished;
        }
    }

    //increment number of processes finished if one has just been finished
    void processFinished() {
        numberOfProcessesFinished++;
    }

    public abstract double simulate(List<Process> processesQueue);


    void addToWaitingTimeSum(int processesWaitTime) {
        waitingTimeSum += processesWaitTime;
    }

    //checks if process arrived within last cycle
    boolean wasProcessAddedWithinLastCycle(Process process, int startOfCycle, int endOfCycle) {
        return (startOfCycle < process.getTimeOfArrival() && process.getTimeOfArrival() <= endOfCycle);
    }

    public double getResults() {
        return (nOfSimulations == 0) ? 0 : simulationsSum/nOfSimulations;
    }

    protected void reset() {
        waitingTimeSum = 0;
        numberOfProcessesFinished = 0;
    }

}
