package miuoshki.ossim.domain.process_scheduling;

import miuoshki.ossim.domain.Process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FCFS extends ProcessSchedulingAlgorithm {


    private int timeWorking = 0;

    //no need to simulate process arrival to queue since it's sorted by time of arrival
    //and processes are executed respectively
    @Override
    public double simulate(List<Process> processesQueue) {

        reset();
        timeWorking = 0;

        //sort processes by their time of arrival
        Collections.sort(processesQueue, Process::compareByArrival);

        while (!processesQueue.isEmpty()) { //as long as there are processes left to execute

            //in case oldest process hasn't come yet, skip to when he arrives
            if (timeWorking < processesQueue.get(0).getTimeOfArrival()) {
                timeWorking = processesQueue.get(0).getTimeOfArrival();
            }

            //retrieve oldest process in queue
            Process currentProcess = processesQueue.get(0);

            //time he waited is equals to time for how long processor has been working
            // from his arrival to start of his execution
            currentProcess.setTimeWaiting(timeWorking - currentProcess.getTimeOfArrival());

            //add his waiting time to sum of all processes waiting times
            super.addToWaitingTimeSum(currentProcess.getTimeWaiting());

            //execute this process, meaning add his time of Cpu usage to processors time working
            timeWorking += currentProcess.getTimeOfCpuUsage();

            //increment number of processes finished
            super.processFinished();

            //remove process from queue since he has been executed
            processesQueue.remove(currentProcess);
        }

        simulationsSum += getAverageWaitingTime();
        nOfSimulations++;
        return super.getAverageWaitingTime();
    }

    @Override
    public String toString() {
        return "First Come, First Served";
    }
}
