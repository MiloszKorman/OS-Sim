package miuoshki.ossim.domain.process_scheduling;

import miuoshki.ossim.domain.Process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SJFWithExpropriation extends ProcessSchedulingAlgorithm {

    private int timeWorking = 0;

    @Override
    public double simulate(List<Process> processesQueue) {

        reset();
        timeWorking = 0;

        List<Process> allProcessesQueue = processesQueue;
        List<Process> availableProcessesQueue = new ArrayList<>();

        //sort all processes by their time of arrival
        Collections.sort(allProcessesQueue, Process::compareByArrival);

        while (!allProcessesQueue.isEmpty()) {

            //clone all processes that arrived within last execution iteration
            // (at start all processes arriving at 0 are cloned)
            allProcessesQueue.forEach(process -> {
                if (wasProcessAddedWithinLastCycle(process, timeWorking - 1, timeWorking)) {
                    availableProcessesQueue.add(process);
                }
            });

            //when all processes haven't been finished yet, but there are currently no processes to execute
            //we skip to fastest arriving process
            if (availableProcessesQueue.isEmpty()) {
                timeWorking = allProcessesQueue.get(0).getTimeOfArrival();
            } else {

                //sort available processes by how long will they take, accordingly to sjf premise
                Collections.sort(availableProcessesQueue, Process::compareByRemainingCpuTime);

                //get quickest to finish process available
                Process currentProcess = availableProcessesQueue.get(0);

                //decrement processes needed time to finish its job
                currentProcess.setRemainingTimeOfCpuUsage(currentProcess.getRemainingTimeOfCpuUsage() - 1);

                //increment cpu's working time
                timeWorking++;

                //set for how long process had to wait so far
                currentProcess.setTimeWaiting(calcProcessesTimeWaiting(currentProcess));

                //if process doesn't need any more time, meaning he has ended
                if (currentProcess.getRemainingTimeOfCpuUsage() == 0) {

                    //add processes time waiting to sum of all processes waiting times
                    super.addToWaitingTimeSum(currentProcess.getTimeWaiting());

                    //increment number of processes finished
                    super.processFinished();

                    //remove him from both queues since he has been finished
                    availableProcessesQueue.remove(currentProcess);
                    allProcessesQueue.remove(currentProcess);

                }

            }

        }

        simulationsSum += getAverageWaitingTime();
        nOfSimulations++;
        return super.getAverageWaitingTime();
    }

    //processes waiting time is time for how long he HASN'T been executed since he arrived
    private int calcProcessesTimeWaiting(Process process) {
        return timeWorking - process.getTimeOfArrival() - (process.getTimeOfCpuUsage() - process.getRemainingTimeOfCpuUsage());
    }

    @Override
    public String toString() {
        return "Shortest Job First with expropriation";
    }
}
