package miuoshki.ossim.domain.process_scheduling;

import miuoshki.ossim.domain.Process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SJFWithoutExpropriation extends ProcessSchedulingAlgorithm {

    private int timeWorking = 0;
    private int timeWorkingStartOfCycle = -1;

    @Override
    public double simulate(List<Process> processesQueue) {

        reset();
        timeWorking = 0;
        timeWorkingStartOfCycle = -1;

        List<Process> allProcessesQueue = processesQueue;
        List<Process> availableProcessesQueue = new ArrayList<>();

        //sort all processes by their time of arrival
        Collections.sort(allProcessesQueue, Process::compareByArrival);


        while (!allProcessesQueue.isEmpty()) { //as long as there are processes left to execute

            //clone all processes that arrived within last execution cycle
            // (at start all processes arriving at 0 are cloned)
            allProcessesQueue.forEach(process -> {
                if (wasProcessAddedWithinLastCycle(process, timeWorkingStartOfCycle, timeWorking)) {
                    availableProcessesQueue.add(process);
                }
            });

            //execution cycle is marked by start of executing certain process and end
            //timeWorkingStartOfCycle stores time of start of a cycle,
            // which is timeWorking when processes execution begins
            timeWorkingStartOfCycle = timeWorking;

            //when all processes haven't been finished yet, but there are currently no processes to execute
            //we skip to fastest arriving process
            if (availableProcessesQueue.isEmpty()) {
                timeWorking = allProcessesQueue.get(0).getTimeOfArrival();
            } else {

                //sort available processes by how long will they take, accordingly to sjf premise
                Collections.sort(availableProcessesQueue, Process::compareByRemainingCpuTime);

                //get shortest available process
                Process currentProcess = availableProcessesQueue.get(0);

                //at start of his execution, his time waiting is time of cpu working - time of his arrival
                currentProcess.setTimeWaiting(timeWorking - currentProcess.getTimeOfArrival());

                //increment cpu's time working, by how long this process will take to finish
                timeWorking += currentProcess.getTimeOfCpuUsage();

                //add processes time waiting to sum of all processes waiting times
                super.addToWaitingTimeSum(currentProcess.getTimeWaiting());

                //increment number of processes finished
                super.processFinished();

                //remove him from both queues since he has been finished
                availableProcessesQueue.remove(currentProcess);
                allProcessesQueue.remove(currentProcess);

            }

        }

        simulationsSum += getAverageWaitingTime();
        nOfSimulations++;
        return super.getAverageWaitingTime();
    }

    @Override
    public String toString() {
        return "Shortest Job First without expropriation";
    }
}
