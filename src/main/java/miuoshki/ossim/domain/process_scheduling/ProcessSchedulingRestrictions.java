package miuoshki.ossim.domain.process_scheduling;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProcessSchedulingRestrictions {

    private int nOfProcesses;
    private int maxCpuTime;
    private int maxArrivalTime;
    private int quantityOfTime;

}
