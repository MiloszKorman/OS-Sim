package miuoshki.ossim.services;

import miuoshki.ossim.domain.Page;
import miuoshki.ossim.domain.paging_in_process.*;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.SplittableRandom;
import java.util.stream.Collectors;

@Service
public class PagingInProcessSimulation {

    private int nOfFrames;
    private int nOfPages;
    private int nOfIncomingPages;
    private int accuracy;
    static SplittableRandom random = new SplittableRandom();

    List<PageReplacementAlgorithm> VMManagerLessFrames;
    List<PageReplacementAlgorithm> VMManager;
    List<PageReplacementAlgorithm> VMManagerMoreFrames;

    List<List<PageReplacementAlgorithm>> listsOfAlgorithms;

    public List<PageReplacementAlgorithm> simulation(PagingInProcessRestrictions restrictions) {

        this.nOfFrames = restrictions.getNOfFrames();
        this.nOfPages = restrictions.getNOfPages();
        this.nOfIncomingPages = restrictions.getNOfIncomingPages();
        this.accuracy = 20;

        initializeReplacementAlgorithms();

        for (int i = 0; i < accuracy; i++) {
            List<Page> incomingPages = generateQueue();

            for (List<PageReplacementAlgorithm> algorithms : listsOfAlgorithms) {
                for (PageReplacementAlgorithm algorithm : algorithms) {
                    algorithm.simulate(new LinkedList<>(incomingPages));
                }
            }
        }

        return listsOfAlgorithms.stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private List<Page> generateQueue() {

        List<Page> incomingPages = new LinkedList<>();

        int nOfLocales = nOfIncomingPages/1000;
        int rest = nOfIncomingPages-nOfLocales;
        int perLocale = (int)Math.ceil(rest/(double)nOfLocales);

        int howManyPages = nOfIncomingPages;

        for (int i = 0; i < nOfLocales; i++) {
            int locale = random.nextInt(1, nOfPages+1);
            incomingPages.add(new Page(locale));
            howManyPages--;
            for (int j = 0; j < perLocale && 0 < howManyPages; j++) {
                int lowerBound = locale - 5;
                if (lowerBound < 1) {
                    lowerBound = 1;
                }
                int upperBound = locale + 5;
                if (nOfPages < upperBound) {
                    upperBound = nOfPages;
                }
                incomingPages.add(new Page(random.nextInt(lowerBound, upperBound+1)));
                howManyPages--;
            }
        }

        return incomingPages;
    }

    private void initializeReplacementAlgorithms() {
        VMManagerLessFrames = new LinkedList<>();
        VMManagerLessFrames.add(new FIFO(nOfFrames/2));
        VMManagerLessFrames.add(new RAND(nOfFrames/2));
        VMManagerLessFrames.add(new ALRU(nOfFrames/2));
        VMManagerLessFrames.add(new LRU(nOfFrames/2));
        VMManagerLessFrames.add(new OPT(nOfFrames/2));

        VMManager = new LinkedList<>();
        VMManager.add(new FIFO(nOfFrames));
        VMManager.add(new RAND(nOfFrames));
        VMManager.add(new ALRU(nOfFrames));
        VMManager.add(new LRU(nOfFrames));
        VMManager.add(new OPT(nOfFrames));

        VMManagerMoreFrames = new LinkedList<>();
        VMManagerMoreFrames.add(new FIFO(nOfFrames*3/2));
        VMManagerMoreFrames.add(new RAND(nOfFrames*3/2));
        VMManagerMoreFrames.add(new ALRU(nOfFrames*3/2));
        VMManagerMoreFrames.add(new LRU(nOfFrames*3/2));
        VMManagerMoreFrames.add(new OPT(nOfFrames*3/2));

        listsOfAlgorithms = new LinkedList<>();
        listsOfAlgorithms.add(VMManagerLessFrames);
        listsOfAlgorithms.add(VMManager);
        listsOfAlgorithms.add(VMManagerMoreFrames);
    }

}
