package miuoshki.ossim.services;

import miuoshki.ossim.domain.Processor;
import miuoshki.ossim.domain.Process;
import miuoshki.ossim.domain.distributed_process_scheduling.*;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DistributedProcessSchedulingSimulation {

    private SplittableRandom random = new SplittableRandom();

    private int nOfProcessors;
    private int maxNOfProcessesPerProcessor;
    private int maxNOfInquiries;
    private int lowerBoundary;
    private int upperBoundary;
    private int accuracy;

    private List<ProcessorAllocationAlgorithm> algorithms;

    public List<ProcessorAllocationAlgorithm> simulation(DistributedProcessSchedulingRestrictions restrictions) {

        this.nOfProcessors = restrictions.getNOfProcessors();
        this.maxNOfProcessesPerProcessor = restrictions.getMaxNOfProcessesPerProcessor();
        this.maxNOfInquiries = restrictions.getMaxNOfInquiries();
        this.lowerBoundary = restrictions.getLowerBoundary();
        this.upperBoundary = restrictions.getUpperBoundary();
        this.accuracy = 1;

        initializeAlgorithms();

        for (int i = 0; i < accuracy; i++) {
            Set<Processor> processors = generateQueue();

            for (ProcessorAllocationAlgorithm algorithm : algorithms) {
                algorithm.simulate(processors.stream().map(Processor::clone).collect(Collectors.toSet()));
            }
        }

        return algorithms;
    }

    private void initializeAlgorithms() {
        algorithms = new LinkedList<>();
        algorithms.add(new XLast(maxNOfInquiries, upperBoundary, lowerBoundary));
        algorithms.add(new XFirst(maxNOfInquiries, upperBoundary, lowerBoundary));
        algorithms.add(new XCaring(maxNOfInquiries, upperBoundary, lowerBoundary));
    }

    private Set<Processor> generateQueue() {
        Set<Processor> processors = new HashSet<>();

        for (int i = 0; i < nOfProcessors; i++) {

            int nOfProcesses = random.nextInt(100, maxNOfProcessesPerProcessor);
            List<Process> processorsProcesses = new ArrayList<>(nOfProcesses);

            for (int j = 0; j < nOfProcesses; j++) {
                processorsProcesses.add(new Process(
                        random.nextInt(0, maxNOfProcessesPerProcessor/4),
                        random.nextInt(1, 10),
                        random.nextInt(1, 50)
                ));
            }

            processors.add(new Processor(processorsProcesses));

        }

        return processors;
    }

}
