package miuoshki.ossim.services;

import miuoshki.ossim.domain.Page;
import miuoshki.ossim.domain.paging_in_processor.*;
import miuoshki.ossim.domain.Process;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.SplittableRandom;
import java.util.stream.Collectors;

@Service
public class PagingInProcessorSimulation {

    private int nOfProcesses;
    private int nOfFrames;
    private int maxPageRequests;
    private int accuracy;

    private SplittableRandom random = new SplittableRandom();

    private List<FrameAllocationAlgorithm> algorithms;

    public List<FrameAllocationAlgorithm> simulation(PagingInProcessorRestrictions restrictions) {

        this.nOfProcesses = restrictions.getNOfProcesses();
        this.nOfFrames = restrictions.getNOfFrames();
        this.maxPageRequests = restrictions.getMaxPageRequests();
        accuracy = 10;

        initializeFrameAllocationAlgorithms();

        for (int i = 0; i < accuracy; i++) {
            List<Process> processes = generateQueue();

            for (FrameAllocationAlgorithm algorithm : algorithms) {
                algorithm.simulate(processes.stream().map(Process::cloneProcess).collect(Collectors.toList()));
            }
        }


        return algorithms;
    }

    private  void initializeFrameAllocationAlgorithms() {
        algorithms = new LinkedList<>();
        algorithms.add(new Equal(nOfFrames));
        algorithms.add(new Proportional(nOfFrames));
        algorithms.add(new WorkingSet(nOfFrames));
        algorithms.add(new PageFaultFrequency(nOfFrames));
    }

    private List<Process> generateQueue() {

        List<Process> processes = new LinkedList<>();
        List<Page> processesPages = new LinkedList<>();

        for (int i = 0; i < nOfProcesses; i++) {

            int nOfIncomingPages = random.nextInt(10, maxPageRequests+1);
            int nOfLocales = nOfIncomingPages/10;
            int rest = nOfIncomingPages - nOfLocales;
            int perLocale = (int)Math.ceil(rest/(double)nOfLocales);

            for (int j = 0; j < nOfLocales; j++) {

                int locale = random.nextInt(1, 1000);
                processesPages.add(new Page(locale));
                nOfIncomingPages--;
                for (int k = 0; k < perLocale && 0 < nOfIncomingPages; k++) {
                    int lowerBound = locale - 10;
                    if (lowerBound < 1) {
                        lowerBound = 1;
                    }
                    int upperBound = locale + 10;
                    if (1000 < upperBound) {
                        upperBound = 1000;
                    }
                    processesPages.add(new Page(random.nextInt(lowerBound, upperBound)));
                    nOfIncomingPages--;
                }

            }

            Process newProcess = new Process(processesPages.size()/10);
            newProcess.setProcessesIncomingPages(new LinkedList<>(processesPages));
            processes.add(newProcess);
            processesPages.clear();
        }

        return processes;
    }

}
