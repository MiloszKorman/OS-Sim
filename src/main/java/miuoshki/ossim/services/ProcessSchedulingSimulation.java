package miuoshki.ossim.services;

import miuoshki.ossim.domain.Process;
import miuoshki.ossim.domain.process_scheduling.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Service
public class ProcessSchedulingSimulation {

    private int nOfProcesses;
    private int maxCpuTime;
    private int maxArrivalTime;
    private int quantityOfTime;
    private int accuracy;

    private List<ProcessSchedulingAlgorithm> algorithms;

    public List<ProcessSchedulingAlgorithm> simulation(ProcessSchedulingRestrictions restrictions) {

        this.nOfProcesses = restrictions.getNOfProcesses();
        this.maxCpuTime = restrictions.getMaxCpuTime();
        this.maxArrivalTime = restrictions.getMaxArrivalTime();
        this.quantityOfTime = restrictions.getQuantityOfTime();
        accuracy = 2;

        algorithms = new LinkedList<>();
        algorithms.addAll(Arrays.asList(
                new FCFS(),
                new SJFWithoutExpropriation(),
                new RR(quantityOfTime),
                new SJFWithExpropriation()
        ));

        for (int i = 0; i < accuracy; i++) {

            List<Process> processesQueue = generate(nOfProcesses, maxCpuTime, maxArrivalTime);

            for (ProcessSchedulingAlgorithm algorithm : algorithms) {
                algorithm.simulate(new ArrayList<>(processesQueue));
                processesQueue.forEach(Process::reset);
            }

        }

        return algorithms;
    }

    private ArrayList<Process> generate(int numberOfProcesses, int maxTimeCpuUsage, int howLate) {

        ArrayList<Process> queue = new ArrayList<>();

        for (int i = 0; i < numberOfProcesses; i++) {

            queue.add(
                    new Process(
                            (int) (Math.random() * (maxTimeCpuUsage - 1)) + 1,
                            (int) (Math.random() * (howLate - 1)) + 1)
            );

        }

        return queue;
    }

}
