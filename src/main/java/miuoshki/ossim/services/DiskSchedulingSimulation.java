package miuoshki.ossim.services;

import miuoshki.ossim.domain.disk_scheduling.DiskSchedulingRestrictions;
import miuoshki.ossim.domain.disk_scheduling.Order;
import miuoshki.ossim.domain.disk_scheduling.PriorityHandlingMethod;
import miuoshki.ossim.domain.disk_scheduling.algorithms.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Service
public class DiskSchedulingSimulation {

    private int startingSector;
    private int queueSize;
    private int howLate;
    private int nOfDeadliners;
    private int maxDeadline;
    private int minDeadline;
    private int accuracy;

    private List<PlanningAlgorithm> algorithms;
    private List<Order> ordersQueue;

    public List<PlanningAlgorithm> simulation(DiskSchedulingRestrictions restrictions) {

        this.startingSector = restrictions.getStartingSector();
        this.queueSize = restrictions.getQueueSize();
        this.howLate = restrictions.getHowLate();
        this.nOfDeadliners = restrictions.getNOfDeadliners();
        this.maxDeadline = restrictions.getMaxDeadline();
        this.minDeadline = restrictions.getMinDeadline();
        this.accuracy = 20;

        algorithms = new LinkedList<>();
        algorithms.addAll(Arrays.asList(
                new FCFS(startingSector, PriorityHandlingMethod.NONE),
                new SSTF(startingSector, PriorityHandlingMethod.NONE),
                new SCAN(startingSector, PriorityHandlingMethod.NONE),
                new CSCAN(startingSector, PriorityHandlingMethod.NONE),
                new FCFS(startingSector, PriorityHandlingMethod.EDF),
                new SSTF(startingSector, PriorityHandlingMethod.EDF),
                new SCAN(startingSector, PriorityHandlingMethod.EDF),
                new CSCAN(startingSector, PriorityHandlingMethod.EDF),
                new FCFS(startingSector, PriorityHandlingMethod.FDSCAN),
                new SSTF(startingSector, PriorityHandlingMethod.FDSCAN),
                new SCAN(startingSector, PriorityHandlingMethod.FDSCAN),
                new CSCAN(startingSector, PriorityHandlingMethod.FDSCAN)
        ));

        ordersQueue = new ArrayList<>();

        for (int i = 0; i < accuracy; i++) {

            generateOrders();

                for (PlanningAlgorithm algorithm : algorithms) {
                    algorithm.simulate(new ArrayList<>(ordersQueue));
                }

        }


        return algorithms;
    }

    private void generateOrders() {

        ordersQueue.clear();

        for (int i = 0; i < queueSize - nOfDeadliners; i++) {
            ordersQueue.add(new Order(
                    (int) (Math.random() * 1000) + 1,
                    (int) (Math.random() * howLate) + 1
            ));
        }

        for (int i = 0; i < nOfDeadliners; i++) {
            ordersQueue.add(new Order(
                    (int) (Math.random() * 1000) + 1,
                    (int) (Math.random() * (howLate + 1)),
                    (int) (Math.random() * (maxDeadline - minDeadline + 1)) + minDeadline
            ));
        }

    }

}
