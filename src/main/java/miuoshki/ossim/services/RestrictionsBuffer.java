package miuoshki.ossim.services;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
public class RestrictionsBuffer {

    private List<Integer> availableKeys;
    private Map<Integer, Object> restrictionsBuffer;
    private int counter;

    public RestrictionsBuffer() {
        availableKeys = new LinkedList<>();
        restrictionsBuffer = new HashMap<>();
        counter = 0;
    }

    public int addRestrictions(Object restrictions) {
        int key;

        if (availableKeys.isEmpty()) {
            key = counter++;
        } else {
            key = availableKeys.remove(0);
        }

        restrictionsBuffer.put(key, restrictions);

        return key+1;
    }

    public Object retrieveRestrictions(int key) {
        availableKeys.add(key-1);

        return restrictionsBuffer.remove(key-1);
    }

}
