package miuoshki.ossim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OsSimApplication {

    public static void main(String[] args) {
        SpringApplication.run(OsSimApplication.class, args);
    }
}
