package miuoshki.ossim.controllers;

import miuoshki.ossim.commands.ProcessSchedulingRestrictionsCommand;
import miuoshki.ossim.converters.ProcessSchedulingRestrictionsCommandToProcessSchedulingRestrictions;
import miuoshki.ossim.domain.process_scheduling.ProcessSchedulingRestrictions;
import miuoshki.ossim.services.ProcessSchedulingSimulation;
import miuoshki.ossim.services.RestrictionsBuffer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class ProcessSchedulingController {

    private final ProcessSchedulingRestrictionsCommandToProcessSchedulingRestrictions converter;
    private final ProcessSchedulingSimulation simulator;
    private final RestrictionsBuffer buffer;

    public ProcessSchedulingController(ProcessSchedulingRestrictionsCommandToProcessSchedulingRestrictions converter,
                                       ProcessSchedulingSimulation simulator, RestrictionsBuffer buffer) {
        this.converter = converter;
        this.simulator = simulator;
        this.buffer = buffer;
    }

    @GetMapping("/simulation/process-scheduling/form")
    public String getRestrictionsForSimulation(Model model) {
        model.addAttribute("process_scheduling_restrictions", new ProcessSchedulingRestrictionsCommand());

        return "simulation/process-scheduling/form";
    }

    @PostMapping("process_scheduling_restrictions")
    public String performSimulation(@Valid @ModelAttribute("process_scheduling_restrictions") ProcessSchedulingRestrictionsCommand restrictionsCommand,
                                    BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> {
                System.out.println(objectError.toString());
            });

            return "simulation/process-scheduling/form";
        }

        return "redirect:/simulation/process-scheduling/"
                + buffer.addRestrictions(converter.convert(restrictionsCommand))
                + "/results";
    }

    @GetMapping("/simulation/process-scheduling/{id}/results")
    public String showResultsOfASimulation(@PathVariable String id, Model model) {
        ProcessSchedulingRestrictions restrictions =
                (ProcessSchedulingRestrictions) buffer.retrieveRestrictions(Integer.valueOf(id));

        model.addAttribute("algorithms", simulator.simulation(restrictions));
        model.addAttribute("restrictions", restrictions);

        return "simulation/process-scheduling/results";
    }

}
