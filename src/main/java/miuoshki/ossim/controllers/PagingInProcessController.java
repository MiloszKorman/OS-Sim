package miuoshki.ossim.controllers;

import miuoshki.ossim.commands.PagingInProcessRestrictionsCommand;
import miuoshki.ossim.converters.PagingInProcessRestrictionsCommandToPagingInProcessRestrictions;
import miuoshki.ossim.domain.paging_in_process.PagingInProcessRestrictions;
import miuoshki.ossim.services.PagingInProcessSimulation;
import miuoshki.ossim.services.RestrictionsBuffer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class PagingInProcessController {

    private final PagingInProcessRestrictionsCommandToPagingInProcessRestrictions converter;
    private final PagingInProcessSimulation simulator;
    private final RestrictionsBuffer buffer;

    public PagingInProcessController(PagingInProcessRestrictionsCommandToPagingInProcessRestrictions converter,
                                     PagingInProcessSimulation simulator, RestrictionsBuffer buffer) {
        this.converter = converter;
        this.simulator = simulator;
        this.buffer = buffer;
    }

    @GetMapping("/simulation/paging-in-process/form")
    public String getRestrictionsForSimulation(Model model) {
        model.addAttribute("paging_in_process_restrictions", new PagingInProcessRestrictionsCommand());

        return "simulation/paging-in-process/form";
    }

    @PostMapping("paging_in_process_restrictions")
    public String performSimulation(@Valid @ModelAttribute("paging_in_process_restrictions") PagingInProcessRestrictionsCommand restrictionsCommand,
                                    BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> {
                System.out.println(objectError.toString());
            });

            return "simulation/paging-in-process/form";
        }

        return "redirect:/simulation/paging-in-process/"
                + buffer.addRestrictions(converter.convert(restrictionsCommand))
                + "/results";
    }

    @GetMapping("/simulation/paging-in-process/{id}/results")
    public String showResultsOfASimulation(@PathVariable String id, Model model) {
        PagingInProcessRestrictions restrictions =
                (PagingInProcessRestrictions) buffer.retrieveRestrictions(Integer.valueOf(id));
        model.addAttribute("algorithms", simulator.simulation(restrictions));
        model.addAttribute("restrictions", restrictions);

        return "simulation/paging-in-process/results";
    }

}
