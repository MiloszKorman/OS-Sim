package miuoshki.ossim.controllers;

import miuoshki.ossim.commands.DistributedProcessSchedulingRestrictionsCommand;
import miuoshki.ossim.converters.DistributedProcessSchedulingRestrictionsCommandToDistributedProcessSchedulingRestrictions;
import miuoshki.ossim.domain.distributed_process_scheduling.DistributedProcessSchedulingRestrictions;
import miuoshki.ossim.services.DistributedProcessSchedulingSimulation;
import miuoshki.ossim.services.RestrictionsBuffer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class DistributedProcessSchedulingController {

    private final DistributedProcessSchedulingRestrictionsCommandToDistributedProcessSchedulingRestrictions converter;
    private final DistributedProcessSchedulingSimulation simulator;
    private final RestrictionsBuffer buffer;

    public DistributedProcessSchedulingController(
            DistributedProcessSchedulingRestrictionsCommandToDistributedProcessSchedulingRestrictions converter,
            DistributedProcessSchedulingSimulation simulator, RestrictionsBuffer buffer) {
        this.converter = converter;
        this.simulator = simulator;
        this.buffer = buffer;
    }

    @GetMapping("/simulation/distributed-process-scheduling/form")
    public String getRestrictionsForSimulation(Model model) {
        model.addAttribute(
                "distributed_processor_scheduling_restrictions",
                new DistributedProcessSchedulingRestrictionsCommand()
        );

        return "simulation/distributed-process-scheduling/form";
    }

    @PostMapping("distributed_processor_scheduling_restrictions")
    public String performSimulation(@Valid @ModelAttribute("distributed_processor_scheduling_restrictions") DistributedProcessSchedulingRestrictionsCommand restrictionsCommand,
                                    BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> {
                System.out.println(objectError.toString());
            });

            return "simulation/distributed-process-scheduling/form";
        }

        return "redirect:/simulation/distributed-process-scheduling/"
                + buffer.addRestrictions(converter.convert(restrictionsCommand))
                +"/results";
    }

    @GetMapping("/simulation/distributed-process-scheduling/{id}/results")
    public String showResultsOfASimulation(@PathVariable String id, Model model) {
        DistributedProcessSchedulingRestrictions restrictions =
                (DistributedProcessSchedulingRestrictions) buffer.retrieveRestrictions(Integer.valueOf(id));
        model.addAttribute("algorithms", simulator.simulation(restrictions));
        model.addAttribute("restrictions", restrictions);

        return "simulation/distributed-process-scheduling/results";
    }

}
