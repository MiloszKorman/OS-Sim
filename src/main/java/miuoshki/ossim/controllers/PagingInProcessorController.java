package miuoshki.ossim.controllers;

import miuoshki.ossim.commands.PagingInProcessorRestrictionsCommand;
import miuoshki.ossim.converters.PagingInProcessorRestrictionsCommandToPagingInProcessorRestrictions;
import miuoshki.ossim.domain.paging_in_processor.PagingInProcessorRestrictions;
import miuoshki.ossim.services.PagingInProcessorSimulation;
import miuoshki.ossim.services.RestrictionsBuffer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class PagingInProcessorController {

    private final PagingInProcessorRestrictionsCommandToPagingInProcessorRestrictions converter;
    private final PagingInProcessorSimulation simulator;
    private final RestrictionsBuffer buffer;

    public PagingInProcessorController(PagingInProcessorRestrictionsCommandToPagingInProcessorRestrictions converter,
                                       PagingInProcessorSimulation simulator, RestrictionsBuffer buffer) {
        this.converter = converter;
        this.simulator = simulator;
        this.buffer = buffer;
    }

    @GetMapping("/simulation/paging-in-processor/form")
    public String getRestrictionsForSimulation(Model model) {
        model.addAttribute("paging_in_processor_restrictions", new PagingInProcessorRestrictions());

        return "simulation/paging-in-processor/form";
    }

    @PostMapping("paging_in_processor_restrictions")
    public String performSimulation(@Valid @ModelAttribute("paging_in_processor_restrictions") PagingInProcessorRestrictionsCommand restrictionsCommand,
                                    BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> {
                System.out.println(objectError.toString());
            });

            return "simulation/paging-in-processor/form";
        }

        return "redirect:/simulation/paging-in-processor/"
                + buffer.addRestrictions(converter.convert(restrictionsCommand))
                + "/results";
    }

    @GetMapping("/simulation/paging-in-processor/{id}/results")
    public String showResultsOfASimulation(@PathVariable String id, Model model) {
        PagingInProcessorRestrictions restrictions =
                (PagingInProcessorRestrictions) buffer.retrieveRestrictions(Integer.valueOf(id));
        model.addAttribute("algorithms", simulator.simulation(restrictions));
        model.addAttribute("restrictions", restrictions);

        return "simulation/paging-in-processor/results";
    }

}
