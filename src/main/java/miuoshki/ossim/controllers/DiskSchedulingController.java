package miuoshki.ossim.controllers;

import miuoshki.ossim.commands.DiskSchedulingRestrictionsCommand;
import miuoshki.ossim.converters.DiskSchedulingRestrictionsCommandToDiskSchedulingRestrictions;
import miuoshki.ossim.domain.disk_scheduling.DiskSchedulingRestrictions;
import miuoshki.ossim.services.DiskSchedulingSimulation;
import miuoshki.ossim.services.RestrictionsBuffer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class DiskSchedulingController {

    private final DiskSchedulingRestrictionsCommandToDiskSchedulingRestrictions converter;
    private final DiskSchedulingSimulation simulator;
    private final RestrictionsBuffer buffer;

    public DiskSchedulingController(DiskSchedulingRestrictionsCommandToDiskSchedulingRestrictions converter,
                                    DiskSchedulingSimulation simulator, RestrictionsBuffer buffer) {
        this.converter = converter;
        this.simulator = simulator;
        this.buffer = buffer;
    }

    @GetMapping("/simulation/disk-scheduling/form")
    public String getRestrictionsForSimulation(Model model) {
        model.addAttribute("disk_scheduling_restrictions", new DiskSchedulingRestrictionsCommand());

        return "simulation/disk-scheduling/form";
    }

    @PostMapping("disk_scheduling_restrictions")
    public String performSimulation(@Valid @ModelAttribute("disk_scheduling_restrictions") DiskSchedulingRestrictionsCommand restrictionsCommand,
                                    BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> {
                System.out.println(objectError.toString());
            });

            return "simulation/disk-scheduling/form";
        }

        return "redirect:/simulation/disk-scheduling/"
                + buffer.addRestrictions(converter.convert(restrictionsCommand))
                + "/results";
    }

    @GetMapping("/simulation/disk-scheduling/{id}/results")
    public String showResultsOfASimualtion(@PathVariable String id, Model model) {
        DiskSchedulingRestrictions restrictions =
                (DiskSchedulingRestrictions) buffer.retrieveRestrictions(Integer.valueOf(id));
        model.addAttribute("algorithms", simulator.simulation(restrictions));
        model.addAttribute("restrictions", restrictions);

        return "simulation/disk-scheduling/results";
    }

}
