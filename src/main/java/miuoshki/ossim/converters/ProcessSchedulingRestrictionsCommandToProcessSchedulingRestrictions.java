package miuoshki.ossim.converters;

import miuoshki.ossim.commands.ProcessSchedulingRestrictionsCommand;
import miuoshki.ossim.domain.process_scheduling.ProcessSchedulingRestrictions;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProcessSchedulingRestrictionsCommandToProcessSchedulingRestrictions implements Converter<ProcessSchedulingRestrictionsCommand, ProcessSchedulingRestrictions> {

    @Override
    public ProcessSchedulingRestrictions convert(ProcessSchedulingRestrictionsCommand source) {

        if (source == null) {
            return null;
        }

        ProcessSchedulingRestrictions restrictions = new ProcessSchedulingRestrictions();
        restrictions.setNOfProcesses(source.getNOfProcesses());
        restrictions.setMaxCpuTime(source.getMaxCpuTime());
        restrictions.setMaxArrivalTime(source.getMaxArrivalTime());
        restrictions.setQuantityOfTime(source.getQuantityOfTime());

        return restrictions;
    }

}
