package miuoshki.ossim.converters;

import miuoshki.ossim.commands.DistributedProcessSchedulingRestrictionsCommand;
import miuoshki.ossim.domain.distributed_process_scheduling.DistributedProcessSchedulingRestrictions;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DistributedProcessSchedulingRestrictionsCommandToDistributedProcessSchedulingRestrictions implements Converter<DistributedProcessSchedulingRestrictionsCommand, DistributedProcessSchedulingRestrictions> {

    @Override
    public DistributedProcessSchedulingRestrictions convert(DistributedProcessSchedulingRestrictionsCommand source) {

        if (source == null) {
            return null;
        }

        DistributedProcessSchedulingRestrictions restrictions = new DistributedProcessSchedulingRestrictions();
        restrictions.setNOfProcessors(source.getNOfProcessors());
        restrictions.setMaxNOfProcessesPerProcessor(source.getMaxNOfProcessesPerProcessor());
        restrictions.setMaxNOfInquiries(source.getMaxNOfInquiries());
        restrictions.setLowerBoundary(source.getLowerBoundary());
        restrictions.setUpperBoundary(source.getUpperBoundary());

        return restrictions;
    }

}
