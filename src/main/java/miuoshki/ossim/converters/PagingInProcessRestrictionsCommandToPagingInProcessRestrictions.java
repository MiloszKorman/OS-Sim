package miuoshki.ossim.converters;

import miuoshki.ossim.commands.PagingInProcessRestrictionsCommand;
import miuoshki.ossim.domain.paging_in_process.PagingInProcessRestrictions;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PagingInProcessRestrictionsCommandToPagingInProcessRestrictions implements Converter<PagingInProcessRestrictionsCommand, PagingInProcessRestrictions> {

    @Override
    public PagingInProcessRestrictions convert(PagingInProcessRestrictionsCommand source) {

        if (source == null) {
            return null;
        }

        PagingInProcessRestrictions restrictions = new PagingInProcessRestrictions();
        restrictions.setNOfFrames(source.getNOfFrames());
        restrictions.setNOfPages(source.getNOfPages());
        restrictions.setNOfIncomingPages(source.getNOfIncomingPages());

        return restrictions;
    }

}
