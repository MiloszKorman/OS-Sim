package miuoshki.ossim.converters;

import miuoshki.ossim.commands.DiskSchedulingRestrictionsCommand;
import miuoshki.ossim.domain.disk_scheduling.DiskSchedulingRestrictions;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DiskSchedulingRestrictionsCommandToDiskSchedulingRestrictions implements Converter<DiskSchedulingRestrictionsCommand, DiskSchedulingRestrictions> {

    @Override
    public DiskSchedulingRestrictions convert(DiskSchedulingRestrictionsCommand source) {

        if (source == null) {
            return null;
        }

        DiskSchedulingRestrictions restrictions = new DiskSchedulingRestrictions();
        restrictions.setStartingSector(source.getStartingSector());
        restrictions.setQueueSize(source.getQueueSize());
        restrictions.setHowLate(source.getHowLate());
        restrictions.setNOfDeadliners(source.getNOfDeadliners());
        restrictions.setMaxDeadline(source.getMaxDeadline());
        restrictions.setMinDeadline(source.getMinDeadline());

        return restrictions;
    }

}
