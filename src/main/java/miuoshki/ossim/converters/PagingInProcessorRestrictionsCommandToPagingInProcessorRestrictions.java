package miuoshki.ossim.converters;

import miuoshki.ossim.commands.PagingInProcessorRestrictionsCommand;
import miuoshki.ossim.domain.paging_in_processor.PagingInProcessorRestrictions;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PagingInProcessorRestrictionsCommandToPagingInProcessorRestrictions implements Converter<PagingInProcessorRestrictionsCommand, PagingInProcessorRestrictions> {

    @Override
    public PagingInProcessorRestrictions convert(PagingInProcessorRestrictionsCommand source) {

        if (source == null) {
            return null;
        }

        PagingInProcessorRestrictions restrictions = new PagingInProcessorRestrictions();
        restrictions.setNOfFrames(source.getNOfFrames());
        restrictions.setNOfProcesses(source.getNOfProcesses());
        restrictions.setMaxPageRequests(source.getMaxPageRequests());

        return restrictions;
    }

}
