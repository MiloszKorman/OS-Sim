package miuoshki.ossim.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@ScriptAssert(lang = "javascript",
        script = "_this.maxArrivalTime <= (_this.NOFProcesses * _this.maxCpuTime)/2",
        message = "Max Arrival Time must be smaller than (number of processes * max cpu time)/2",
        reportOn = "maxArrivalTime")
@ScriptAssert(lang = "javascript",
        script = "_this.quantityOfTime <= _this.maxCpuTime",
        message = "Quantity of Time must be smaller than or equal to maxCpuTime",
        reportOn = "quantityOfTime")
public class ProcessSchedulingRestrictionsCommand {

    @NotNull
    @Min(1)
    @Max(1000)
    private Integer nOfProcesses;

    @NotNull
    @Min(1)
    @Max(1000)
    private Integer maxCpuTime;

    @NotNull
    @Min(1)
    private Integer maxArrivalTime;

    @NotNull
    @Min(1)
    private Integer quantityOfTime;

}
