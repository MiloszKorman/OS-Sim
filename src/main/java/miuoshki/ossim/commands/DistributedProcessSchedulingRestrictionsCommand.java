package miuoshki.ossim.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@ScriptAssert(lang = "javascript",
        script = "_this.maxNOfInquiries <= _this.maxNOfProcessesPerProcessor",
        message = "Max number of inquiries cannot be greater than max number of processes per processor",
        reportOn = "maxNOfInquiries")
public class DistributedProcessSchedulingRestrictionsCommand {

    @NotNull
    @Min(50)
    @Max(100)
    private Integer nOfProcessors;

    @NotNull
    @Min(500)
    @Max(10000)
    private Integer maxNOfProcessesPerProcessor;

    @NotNull
    @Min(1)
    private Integer maxNOfInquiries;

    @NotNull
    @Min(1)
    @Max(49)
    private Integer lowerBoundary;

    @NotNull
    @Min(50)
    @Max(100)
    private Integer upperBoundary;

}
