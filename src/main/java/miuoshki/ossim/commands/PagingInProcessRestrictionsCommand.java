package miuoshki.ossim.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@ScriptAssert(lang = "javascript",
        script = "_this.nOfFrames <= _this.nOfPages",
        message = "Number of Pages must be at least as big as Number of Frames",
        reportOn = "nOfPages")
public class PagingInProcessRestrictionsCommand {

    @NotNull
    @Min(3)
    @Max(20)
    private Integer nOfFrames;

    @NotNull
    @Max(100)
    private Integer nOfPages;

    @NotNull
    @Min(1000)
    @Max(10000)
    private Integer nOfIncomingPages;

}
