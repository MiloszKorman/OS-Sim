package miuoshki.ossim.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.*;

@Getter
@Setter
@NoArgsConstructor
@ScriptAssert(lang = "javascript", script = "_this.NOfDeadliners < _this.queueSize", message = "Number of orders with deadline must be smaller than queue size", reportOn = "NOfDeadliners")
@ScriptAssert(lang = "javascript", script = "_this.howLate < _this.queueSize * 250", message = "How late must be smaller than queue size * 250", reportOn = "howLate")
@ScriptAssert(lang = "javascript", script = "_this.minDeadline <= _this.maxDeadline", message = "Max Deadline must be greater than or equal to Min Deadline", reportOn = "maxDeadline")
public class DiskSchedulingRestrictionsCommand {

    @NotNull
    @Min(1)
    @Max(1000)
    private Integer startingSector;

    @NotNull
    @Min(1)
    @Max(1000)
    private Integer queueSize;

    @NotNull
    @Min(0)
    private Integer howLate;

    @NotNull
    @Min(0)
    private Integer nOfDeadliners;

    @NotNull
    @Max(2000)
    private Integer maxDeadline;

    @NotNull
    @Min(1)
    @Max(999)
    private Integer minDeadline;

}
