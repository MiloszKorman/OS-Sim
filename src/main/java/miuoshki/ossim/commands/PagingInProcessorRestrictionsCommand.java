package miuoshki.ossim.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@ScriptAssert(lang = "javascript", script = "_this.nOfProcesses * 10 <= _this.nOfFrames", message = "Number of frames must be greater than number of processes * 10", reportOn = "nOfFrames")
public class PagingInProcessorRestrictionsCommand {

    @NotNull
    @Min(10)
    @Max(100)
    private Integer nOfProcesses;

    @NotNull
    @Max(1000)
    private Integer nOfFrames;

    @NotNull
    @Min(10)
    @Max(10000)
    private Integer maxPageRequests;

}
