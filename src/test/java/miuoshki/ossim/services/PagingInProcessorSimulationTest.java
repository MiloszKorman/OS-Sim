package miuoshki.ossim.services;

import miuoshki.ossim.domain.paging_in_processor.FrameAllocationAlgorithm;
import miuoshki.ossim.domain.paging_in_processor.PagingInProcessorRestrictions;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class PagingInProcessorSimulationTest {

    @Test
    public void pagingInProcessorSimulationTest() {
        PagingInProcessorSimulation simulation = new PagingInProcessorSimulation();

        PagingInProcessorRestrictions restrictions = new PagingInProcessorRestrictions();
        restrictions.setNOfFrames(20);
        restrictions.setNOfProcesses(100);
        restrictions.setMaxPageRequests(1000);

        List<FrameAllocationAlgorithm> algorithms = simulation.simulation(restrictions);
        assertEquals(4, algorithms.size());
        for (FrameAllocationAlgorithm algorithm : algorithms) {
            assertTrue(0 < algorithm.getAvgNumOfGlobalPageErrors());
            assertTrue(0 < algorithm.getAvgNumOfPageErrorsPerProcess());
        }
    }

}