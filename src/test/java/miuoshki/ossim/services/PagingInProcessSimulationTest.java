package miuoshki.ossim.services;

import miuoshki.ossim.domain.paging_in_process.PageReplacementAlgorithm;
import miuoshki.ossim.domain.paging_in_process.PagingInProcessRestrictions;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class PagingInProcessSimulationTest {

    @Test
    public void pagingInProcessSimulationTest() {
        PagingInProcessSimulation simulation = new PagingInProcessSimulation();

        PagingInProcessRestrictions restrictions = new PagingInProcessRestrictions();
        restrictions.setNOfFrames(30);
        restrictions.setNOfPages(100);
        restrictions.setNOfIncomingPages(1000);

        List<PageReplacementAlgorithm> algorithms = simulation.simulation(restrictions);
        assertEquals(15, algorithms.size());
        for (PageReplacementAlgorithm algorithm : algorithms) {
            assertTrue(!String.valueOf(algorithm).isEmpty());
            assertTrue(0 < algorithm.getAveragePageErrors());
        }
    }

}