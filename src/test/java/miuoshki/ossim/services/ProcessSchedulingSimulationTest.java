package miuoshki.ossim.services;

import miuoshki.ossim.domain.process_scheduling.ProcessSchedulingAlgorithm;
import miuoshki.ossim.domain.process_scheduling.ProcessSchedulingRestrictions;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ProcessSchedulingSimulationTest {

    @Test
    public void processSchedulingSimulationTest() {
        ProcessSchedulingSimulation simulation = new ProcessSchedulingSimulation();

        ProcessSchedulingRestrictions restrictions = new ProcessSchedulingRestrictions();
        restrictions.setNOfProcesses(500);
        restrictions.setMaxCpuTime(600);
        restrictions.setMaxArrivalTime(1000);
        restrictions.setQuantityOfTime(100);

        List<ProcessSchedulingAlgorithm> algorithms = simulation.simulation(restrictions);
        assertEquals(4, algorithms.size());
        for (ProcessSchedulingAlgorithm algorithm : algorithms) {
            assertTrue(!String.valueOf(algorithm).isEmpty());
            assertTrue(0 < algorithm.getResults());
        }
    }

}