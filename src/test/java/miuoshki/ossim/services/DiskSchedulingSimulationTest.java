package miuoshki.ossim.services;

import miuoshki.ossim.domain.disk_scheduling.DiskSchedulingRestrictions;
import miuoshki.ossim.domain.disk_scheduling.algorithms.PlanningAlgorithm;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class DiskSchedulingSimulationTest {

    @Test
    public void diskSchedulingSimulationTest() {
        DiskSchedulingSimulation simulation = new DiskSchedulingSimulation();

        DiskSchedulingRestrictions restrictions = new DiskSchedulingRestrictions();
        restrictions.setQueueSize(600);
        restrictions.setHowLate(1000);
        restrictions.setNOfDeadliners(100);
        restrictions.setMinDeadline(10);
        restrictions.setMaxDeadline(200);

        List<PlanningAlgorithm> algorithms = simulation.simulation(restrictions);
        assertEquals(3*4, algorithms.size());
        for (PlanningAlgorithm algorithm : algorithms) {
            assertTrue(0 < algorithm.getAverageHeadMoves());
        }
    }

}