package miuoshki.ossim.services;

import miuoshki.ossim.domain.distributed_process_scheduling.DistributedProcessSchedulingRestrictions;
import miuoshki.ossim.domain.distributed_process_scheduling.ProcessorAllocationAlgorithm;
import miuoshki.ossim.domain.distributed_process_scheduling.Stats;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class DistributedProcessSchedulingSimulationTest {

    @Test
    public void distributedProcessSchedulingSimulationTest() {
        DistributedProcessSchedulingSimulation simulation = new DistributedProcessSchedulingSimulation();

        DistributedProcessSchedulingRestrictions restrictions = new DistributedProcessSchedulingRestrictions();
        restrictions.setNOfProcessors(40);
        restrictions.setMaxNOfProcessesPerProcessor(500);
        restrictions.setMaxNOfInquiries(400);
        restrictions.setLowerBoundary(20);
        restrictions.setUpperBoundary(80);

        List<ProcessorAllocationAlgorithm> algorithms = simulation.simulation(restrictions);
        assertEquals(3, algorithms.size());
        for (ProcessorAllocationAlgorithm algorithm : algorithms) {
            Stats algorithmsStats = algorithm.getStats();
            assertTrue(0 < algorithmsStats.getNumberOfInteractions());
            assertTrue(0 < algorithmsStats.getAvgProcessorsUsage());
            assertTrue(0 < algorithmsStats.getAvgDeviation());
        }
    }

}