package miuoshki.ossim.domain;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class ProcessTest {

    @Before
    public void setUp() throws NoSuchFieldException, IllegalAccessException {
        Field field = Process.class.getDeclaredField("numberOfProcesses");
        field.setAccessible(true);
        field.set(null, 0);
    }

    @Test
    //checks whether assigning id based on in application creation works correctly
    public void testProcessCreation() {

        Process process1 = new Process(5, 1);
        Process process2 = new Process(6, 2);
        Process process3 = new Process(2, 0);

        assertEquals(1, process1.getId());
        assertEquals(2, process2.getId());
        assertEquals(3, process3.getId());

    }

}