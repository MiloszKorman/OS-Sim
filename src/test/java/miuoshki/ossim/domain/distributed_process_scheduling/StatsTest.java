package miuoshki.ossim.domain.distributed_process_scheduling;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class StatsTest {

    Stats stats;

    Map<Integer, List<Integer>> historyOfUsage;

    @Before
    public void setUp() {
        historyOfUsage = new HashMap<>();
        historyOfUsage.put(1,Arrays.asList(1, 2, 3, 4, 5));
        historyOfUsage.put(2, Arrays.asList(3, 7, 1));
        historyOfUsage.put(3, Arrays.asList(12, 5, 13));

        stats = new Stats();
        stats.addAll(historyOfUsage, 0, 0);
    }

    @Test
    public void getAverageProcessorUsage() {
        double avgProcessorUsage = stats.getAvgProcessorsUsage();
        assertTrue(5.55 < avgProcessorUsage && avgProcessorUsage < 5.56);
    }

    @Test
    public void getAverageDeviation() {
        double avgDeviation = stats.getAvgDeviation();
        assertTrue(3.4 < avgDeviation && avgDeviation < 3.41);
    }

    @Test
    public void testDifference() {
        Map<Integer, List<Integer>> historyOfUsage1 = new HashMap<>();
        historyOfUsage1.put(1, Arrays.asList(20, 4, 0));
        historyOfUsage1.put(2, Arrays.asList(14, 11, 5));
        historyOfUsage1.put(3, Arrays.asList(13, 0, 0));

        Map<Integer, List<Integer>> historyOfUsage2 = new HashMap<>();
        historyOfUsage2.put(1, Arrays.asList(7, 7, 7));
        historyOfUsage2.put(2, Arrays.asList(5, 4, 3));
        historyOfUsage2.put(3, Arrays.asList(1, 0, 2));
        historyOfUsage2.put(4, Arrays.asList(10, 2, 3));
        historyOfUsage2.put(5, Arrays.asList(10, 2, 4));

        Stats stats1 = new Stats();
        stats1.addAll(historyOfUsage1, 10, 10);
        Stats stats2 = new Stats();
        stats2.addAll(historyOfUsage2, 10, 10);

        assertNotEquals(stats1.getAvgProcessorsUsage(), stats2.getAvgProcessorsUsage());
    }

}