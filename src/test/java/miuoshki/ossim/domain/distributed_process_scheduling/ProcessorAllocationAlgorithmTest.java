package miuoshki.ossim.domain.distributed_process_scheduling;

import miuoshki.ossim.domain.Processor;
import miuoshki.ossim.domain.Process;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class ProcessorAllocationAlgorithmTest {

    @Test
    public void testOneProcessor() {

        List<Process> processorsProcesses = Arrays.asList(
                new Process(1, 25, 10000),
                new Process(1, 25, 10000),
                new Process(1, 25, 10000),
                new Process(1, 25, 10000)
        );

        List<Processor> processors = new LinkedList<>();
        processors.add(new Processor(processorsProcesses));

        List<ProcessorAllocationAlgorithm> algorithms = new LinkedList<>();
        algorithms.add(new XLast(100, 80, 20));
        algorithms.add(new XFirst(100, 80, 20));
        algorithms.add(new XCaring(100, 80, 20));

        for (ProcessorAllocationAlgorithm algorithm : algorithms) {
            algorithm.simulate(processors.stream().map(Processor::clone).collect(Collectors.toSet()));
            Stats algorithmsStats = algorithm.getStats();
            assertTrue(99 < algorithmsStats.getAvgProcessorsUsage());
            assertTrue(algorithmsStats.getAvgDeviation() < 1);
            assertEquals(0, algorithmsStats.getNumberOfInteractions());
        }

    }

    @Test
    public void testTwoProcessors() {
        List<Processor> processors = new LinkedList<>(Arrays.asList(
                new Processor(new LinkedList<>(Arrays.asList(
                        new Process(1, 25, 10000),
                        new Process(1, 25, 10000)
                ))), new Processor(new LinkedList<>(Arrays.asList(
                        new Process(1, 25, 10000),
                        new Process(1, 25, 10000),
                        new Process(1, 25, 10000),
                        new Process(1, 25, 10000),
                        new Process(1, 25, 10000),
                        new Process(1, 25, 10000)
                )))
        ));

        List<ProcessorAllocationAlgorithm> algorithms = new LinkedList<>();
        algorithms.add(new XLast(100, 80, 20));
        algorithms.add(new XFirst(100, 80, 20));
        algorithms.add(new XCaring(100, 80, 20));

        for (ProcessorAllocationAlgorithm algorithm : algorithms) {
            algorithm.simulate(processors.stream().map(Processor::clone).collect(Collectors.toSet()));
            Stats stats = algorithm.getStats();
            assertTrue(99 < stats.getAvgProcessorsUsage());
            assertTrue(stats.getAvgDeviation() < 1);
            if (algorithm instanceof XLast && !(algorithm instanceof XFirst)) {
                assertEquals(14, stats.getNumberOfInteractions());
            }
            if (algorithm instanceof XFirst) {
                assertEquals(4, stats.getNumberOfInteractions());
            }
        }
    }

    @Test
    public void testNotTooManyInteractionsInXLast() {
        Set<Processor> processors = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            processors.add(new Processor(new LinkedList<>(Arrays.asList(new Process(0, 1, 10000)))));
        }
        XLast xLast = new XLast(100, 100, 40);
        xLast.simulate(processors);
        Stats stats = xLast.getStats();
        assertTrue(0.98 < stats.getAvgProcessorsUsage() && stats.getAvgProcessorsUsage() <= 1);
        assertEquals(200, stats.getNumberOfInteractions());
    }

    @Test
    public void testProperNumberOfInteractionsInXFirst() {

        List<Process> loadedProcesses = new LinkedList<>();

        for(int i = 0; i < 100; i++) {
            loadedProcesses.add(new Process(0, 1, 1000));
        }

        Processor loaded = new Processor(loadedProcesses);
        loaded.setCurrentUsage(101);

        Set<Processor> processors = new HashSet<>(Arrays.asList(
                loaded,
                new Processor(new LinkedList<>())
        ));

        XFirst xFirst = new XFirst(100, 100, 1);
        xFirst.simulate(processors);
        Stats stats = xFirst.getStats();

        assertEquals(200, stats.getNumberOfInteractions());
    }

}