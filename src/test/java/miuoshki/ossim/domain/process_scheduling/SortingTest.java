package miuoshki.ossim.domain.process_scheduling;

import miuoshki.ossim.domain.Process;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class SortingTest {

    private ArrayList<Process> processesQueue = new ArrayList<>();

    private Process process1;
    private Process process2;
    private Process process3;
    private Process process4;

    @Before
    public void setUp() {
        processesQueue.add(process1 = new Process(12, 5));
        processesQueue.add(process2 = new Process(26, 3));
        processesQueue.add(process3 = new Process(20, 0));
        processesQueue.add(process4 = new Process(14, 8));
    }

    @Test
    public void timeOfArrivalSortTest() {
        Collections.sort(processesQueue, Process::compareByArrival);
        assertEquals(Arrays.asList(process3, process2, process1, process4), processesQueue);
    }

    @Test
    public void timeOfCpuUsageTest() {
        Collections.sort(processesQueue, Process::compareByRemainingCpuTime);
        assertEquals(Arrays.asList(process1, process4, process3, process2), processesQueue);
    }

}
