package miuoshki.ossim.domain.process_scheduling;

import miuoshki.ossim.domain.Process;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ProcessSchedulingAlgorithmTest {

    @Test
    public void FCFSSimulationTest() {

        //test simulations behaviour with just one process in queue
        FCFS fcfs = new FCFS();
        //(0)/1=0
        assertEquals(0, fcfs.simulate(createOneProcessQueue()), 0.1);

        //test simulations behaviour with normal processes queue
        fcfs = new FCFS();
        //(0+28+33+38)/4=24.75
        assertEquals(24.75, fcfs.simulate(createNormalQueue()), 0.01);

        //test simulations behaviour with queue of processes, where there are empty spots between end of executing
        //one process and arrival of another one
        fcfs = new FCFS();
        //(0+0+1+0)/4=0.25
        assertEquals(0.25, fcfs.simulate(createDelayedQueue()), 0.01);

        //tests simulations behaviour with queue in which shorter processes come within cycles
        fcfs = new FCFS();
        //(0+10+20+16)/4=11.5
        assertEquals(11.5, fcfs.simulate(createSJFTestCase()), 0.1);
    }

    @Test
    public void SJFWithoutSimulationTest() {

        //tests simulations behaviour with just one process in queue
        SJFWithoutExpropriation sjfWithout = new SJFWithoutExpropriation();
        //(0)/1=0
        assertEquals(0, sjfWithout.simulate(createOneProcessQueue()), 0.1);

        //tests simulations behaviour with normal processes queue
        sjfWithout = new SJFWithoutExpropriation();
        //(0+5+10+26)/4=10.25
        assertEquals(10.25, sjfWithout.simulate(createNormalQueue()), 0.01);

        //test simulations behaviour with queue of processes, where there are empty spots between end of executing
        //one process and arrival of another one
        sjfWithout = new SJFWithoutExpropriation();
        //(0+0+1+0)/4=0.25
        assertEquals(0.25, sjfWithout.simulate(createDelayedQueue()), 0.01);

        //tests simulations behaviour with shorter processes coming within cycles
        sjfWithout = new SJFWithoutExpropriation();
        //(0+17+5+1)/4=5.75
        assertEquals(5.75, sjfWithout.simulate(createSJFTestCase()), 0.01);
    }

    @Test
    public void SJFWithSimulationTest() {

        //tests simulations behaviour with just one process in queue
        SJFWithExpropriation sjfWith = new SJFWithExpropriation();
        //(0)/1=0
        assertEquals(0, sjfWith.simulate(createOneProcessQueue()), 0.1);

        //tests whether average time for regular queue and normal quantity is correct
        sjfWith = new SJFWithExpropriation();
        //(26+0+5+10)/4=10.25
        assertEquals(10.25, sjfWith.simulate(createNormalQueue()), 0.01);

        //test simulations behaviour with queue of processes, where there are empty spots between end of executing
        //one process and arrival of another one
        sjfWith = new SJFWithExpropriation();
        //(0+0+1+0)/4=0.25
        assertEquals(0.25, sjfWith.simulate(createDelayedQueue()), 0.01);

        //tests simulations behaviour with queue in which shorter processes come within cycles
        sjfWith = new SJFWithExpropriation();
        //(3+17+0+1)/4=5.25
        assertEquals(5.25, sjfWith.simulate(createSJFTestCase()), 0.01);
    }

    @Test
    public void RRSimulationTest() {

        //tests simulations behaviour with just one process in queue
        RR rr = new RR(5);
        //(0)/1=0
        assertEquals(0, rr.simulate(createOneProcessQueue()), 0.1);

        //tests whether average time for regular queue and normal quantity is correct
        rr = new RR(8);
        //(26+8+13+26)/4=18.25
        assertEquals(18.25, rr.simulate(createNormalQueue()), 0.01);

        //tests whether average time for regular queue and small quantity is correct
        rr = new RR(2);
        //(26+14+15+26)/4=20.25
        assertEquals(20.25, rr.simulate(createNormalQueue()), 0.01);

        //test simulations behaviour with queue of processes, where there are empty spots between end of executing
        //one process and arrival of another one
        rr = new RR(4);
        //(0+0+1+0)/4=0.25
        assertEquals(0.25, rr.simulate(createDelayedQueue()), 0.01);

        //tests whether average time for queue with shorter processes coming within cycles is correct
        rr = new RR(10);
        //(0+17+15+11)/4=10.75
        assertEquals(10.75, rr.simulate(createSJFTestCase()), 0.01);
    }

    private static ArrayList<Process> createOneProcessQueue() {
        ArrayList<Process> queue = new ArrayList<>();

        queue.add(new Process(5, 0));

        return queue;
    }

    //creating regular processes queue without any empty cycles
    private static ArrayList<Process> createNormalQueue() {
        ArrayList<Process> queue = new ArrayList<>();

        queue.add(new Process(28, 0));
        queue.add(new Process(5, 0));
        queue.add(new Process(6, 0));
        queue.add(new Process(15, 1));

        return queue;
    }

    //creating processes queue causing empty cycles
    private static ArrayList<Process> createDelayedQueue() {
        ArrayList<Process> queue = new ArrayList<>();

        queue.add(new Process(5, 0));
        queue.add(new Process(3, 10));
        queue.add(new Process(6, 12));
        queue.add(new Process(4, 21));

        return queue;
    }

    //creating processes queue in which shorter processes come within cpu cycles
    private static ArrayList<Process> createSJFTestCase() {
        ArrayList<Process> queue = new ArrayList<>();

        queue.add(new Process(10, 0));
        queue.add(new Process(15, 0));
        queue.add(new Process(3, 5));
        queue.add(new Process(4, 12));

        return queue;
    }

}