package miuoshki.ossim.domain.paging_in_processor;

import miuoshki.ossim.domain.Page;
import miuoshki.ossim.domain.Process;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class FrameAllocationAlgorithmTest {

    @Test
    public void EqualAllocationTest() {
        //5*1 + 4*2 + 3*1 + 2*1 + 2*1
        assertEquals(20, (new Equal(15)).simulate(createTestsCase()));
    }

    @Test
    public void ProportionalAllocationTest() {
        //5*1 + 4*1 + 4*1 + 3*1 + 2*1 + 1*1 + 1*1
        assertEquals(22, (new Proportional(15)).simulate(createTestsCase()));
    }

    @Test
    public void WorkingSetAllocationTest() {
        //7 + 6 + 4 + 3 + 1
        assertEquals(21, (new WorkingSet(15)).simulate(createTestsCase()));
    }

    @Test
    public void workingSetsHistoryQueueTest() {
        WorkingSet.Queue queue = new WorkingSet.Queue();
        queue.add(new Page(0));
        queue.add(new Page(1));
        queue.add(new Page(2));
        queue.add(new Page(3));
        queue.add(new Page(1));
        queue.add(new Page(1));
        queue.add(new Page(1));
        queue.add(new Page(1));
        queue.add(new Page(1));
        queue.add(new Page(1));
        queue.add(new Page(4));
        assertEquals(4, queue.uniqueSize());
    }

    @Test
    public void PageFaultFrequencyTest() {
        //6 + 6 + 4 + 3 + 1
        assertEquals(20, (new PageFaultFrequency(15)).simulate(createTestsCase()));
    }

    private List<Process> createTestsCase() {
        List<Process> testCase = new LinkedList<>();

        testCase.add(new Process(new LinkedList<>(Arrays.asList(
                new Page(7),
                new Page(0),
                new Page(1),
                new Page(2),
                new Page(0),
                new Page(3),
                new Page(0),
                new Page(4),
                new Page(2),
                new Page(3),
                new Page(0),
                new Page(3),
                new Page(2),
                new Page(1),
                new Page(2),
                new Page(0),
                new Page(1),
                new Page(7),
                new Page(0),
                new Page(1)
        )), 3));
        testCase.add(new Process(new LinkedList<>(Arrays.asList(
                new Page(7),
                new Page(0),
                new Page(1),
                new Page(2),
                new Page(0),
                new Page(3),
                new Page(0),
                new Page(4),
                new Page(2),
                new Page(3)
        )), 3));
        testCase.add(new Process(new LinkedList<>(Arrays.asList(
                new Page(7),
                new Page(0),
                new Page(1),
                new Page(2),
                new Page(0)
        )), 3));
        testCase.add(new Process(new LinkedList<>(Arrays.asList(
                new Page(7),
                new Page(0),
                new Page(1)
        )), 3));
        testCase.add(new Process(new LinkedList<>(Arrays.asList(
                new Page(7)
        )), 1));

        return testCase;
    }

}