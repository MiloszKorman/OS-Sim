package miuoshki.ossim.domain.paging_in_processor;

import miuoshki.ossim.domain.Page;
import miuoshki.ossim.domain.Process;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class ProcessTest {

    @Test
    public void cloneTest() {
        Process process = new Process(15);
        process.setProcessesIncomingPages(Arrays.asList(
                new Page(3),
                new Page(2),
                new Page(1)
        ));
        assertEquals(3, process.cloneProcess().getSize());
    }

}
