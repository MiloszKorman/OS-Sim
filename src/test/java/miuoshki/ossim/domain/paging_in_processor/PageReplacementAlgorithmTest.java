package miuoshki.ossim.domain.paging_in_processor;

import miuoshki.ossim.domain.Page;
import miuoshki.ossim.domain.paging_in_process.LRU;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PageReplacementAlgorithmTest {

    @Test
    public void LRUTest() {
        assertEquals(12, (new LRU(3)).simulate(
                bookTestQueue()
        ));
    }

    @Test
    public void LRUChangingFrameTest() {
        LRU lru = new LRU();
        lru.simulateIteration(new Page(7), 3);
        assertEquals(1, lru.getPageErrors());
        lru.simulateIteration(new Page(0), 3);
        assertEquals(2, lru.getPageErrors());
        lru.simulateIteration(new Page(1), 3);
        assertEquals(3, lru.getPageErrors());
        lru.simulateIteration(new Page(2), 2);
        assertEquals(4, lru.getPageErrors());
        lru.simulateIteration(new Page(0), 2);
        assertEquals(5, lru.getPageErrors());
        lru.simulateIteration(new Page(3), 1);
        assertEquals(6, lru.getPageErrors());
        lru.simulateIteration(new Page(0), 1);
        assertEquals(7, lru.getPageErrors());
        lru.simulateIteration(new Page(4), 3);
        assertEquals(8, lru.getPageErrors());
        lru.simulateIteration(new Page(2), 3);
        assertEquals(9, lru.getPageErrors());
        lru.simulateIteration(new Page(3), 3);
        assertEquals(10, lru.getPageErrors());
        lru.simulateIteration(new Page(0), 3);
        assertEquals(11, lru.getPageErrors());
        lru.simulateIteration(new Page(3), 2);
        assertEquals(11, lru.getPageErrors());
        lru.simulateIteration(new Page(2), 2);
        assertEquals(12, lru.getPageErrors());
        lru.simulateIteration(new Page(1), 1);
        assertEquals(13, lru.getPageErrors());
        lru.simulateIteration(new Page(2), 1);
        assertEquals(14, lru.getPageErrors());
        lru.simulateIteration(new Page(0), 3);
        assertEquals(15, lru.getPageErrors());
        lru.simulateIteration(new Page(1), 3);
        assertEquals(16, lru.getPageErrors());
        lru.simulateIteration(new Page(7), 3);
        assertEquals(17, lru.getPageErrors());
        lru.simulateIteration(new Page(0), 2);
        assertEquals(17, lru.getPageErrors());
        lru.simulateIteration(new Page(1), 2);
        assertEquals(18, lru.getPageErrors());
    }

    private List<Page> bookTestQueue() {
        List<Page> testingQueue = Arrays.asList(
                new Page(7),
                new Page(0),
                new Page(1),
                new Page(2),
                new Page(0),
                new Page(3),
                new Page(0),
                new Page(4),
                new Page(2),
                new Page(3),
                new Page(0),
                new Page(3),
                new Page(2),
                new Page(1),
                new Page(2),
                new Page(0),
                new Page(1),
                new Page(7),
                new Page(0),
                new Page(1)
        );

        return new ArrayList<>(testingQueue);
    }

}
