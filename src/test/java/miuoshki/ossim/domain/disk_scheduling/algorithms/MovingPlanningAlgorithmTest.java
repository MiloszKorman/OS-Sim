package miuoshki.ossim.domain.disk_scheduling.algorithms;

import miuoshki.ossim.domain.disk_scheduling.Order;
import miuoshki.ossim.domain.disk_scheduling.PriorityHandlingMethod;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class MovingPlanningAlgorithmTest {



    @Test
    public void simpleFCFSTest() {
        //|98-53| + |183-98| + |37-183| + |122-37| + |14-122| + |124-14| + |65-124| + |67-65| = 640
        assertEquals(640, (new FCFS(53)).simulate(createTestOrders()));
    }

    @Test
    public void arrivingFCFSTest() {
        //0 -> 11 -> 63 -> 840 -> 1531 -> 1582 -> 1727 -> 1828 -> 1877 -> 1945 -> 2805 -> 3734
        assertEquals(3734, (new FCFS(53)).simulate(createArrivingTestOrders()));
    }

    @Test
    public void EDFFCFSTest() {
        FCFS edf = new FCFS(53, PriorityHandlingMethod.EDF);
        assertEquals(2990, edf.simulate(createPriorityTestOrders()));
        assertEquals(2, edf.howManyPastDeadline());
    }

    @Test
    public void FDSCANFCFSTest() {
        assertEquals(1868, (new FCFS(53, PriorityHandlingMethod.FDSCAN)).simulate(createPriorityTestOrders()));
    }

    @Test
    public void simpleSSTFTest() {
        //|65-53| + |67-65| + |37-67| + |14-37| + |98-14| + |122-98| + |124-122| + |183-124| = 236
        assertEquals(236, (new SSTF(53)).simulate(createTestOrders()));
    }

    @Test
    public void arrivingSSTFTest() {
        assertEquals(2248, (new SSTF(53)).simulate(createArrivingTestOrders()));
    }

    @Test
    public void sneakyOrderSSTFTest() {
        //53i -> 300i -> 500i => 447
        assertEquals(447, (new SSTF(53)).simulate(sneakyOrder()));
    }

    @Test
    public void EDFSSTFTest() {
        SSTF edfSstf = new SSTF(53, PriorityHandlingMethod.EDF);
        assertEquals(2608, edfSstf.simulate(createPriorityTestOrders()));
        assertEquals(2, edfSstf.howManyPastDeadline());
    }

    @Test
    public void simpleSCANTest() {
        //|65-53| + |67-65| + |98-67| + |122-98| + |124-122| + |183-124| + |1000-183| + |37-1000| + |14-37| = 1933
        assertEquals(1933, (new SCAN(53)).simulate(createTestOrders()));
    }

    @Test
    public void arrivingSCANTest() {
        //0 -> 11 -> 96 -> 736 -> 963 -> 1823 -> 1842 -> 1849 -> 1891 -> 1892 -> 1935 -> 1943
        assertEquals(1943, (new SCAN(53)).simulate(createArrivingTestOrders()));
    }

    @Test
    public void sneakySCANTest() {
        //53i -> 300i -> 500i => 447
        assertEquals(447, (new SCAN(53)).simulate(sneakyOrder()));
    }

    @Test
    public void EDFSCANTest() {
        SCAN edfScan = new SCAN(53, PriorityHandlingMethod.EDF);
        assertEquals(2072, edfScan.simulate(createPriorityTestOrders()));
        assertEquals(2, edfScan.howManyPastDeadline());
    }

    @Test
    public void FDSCANSCANTest() {
        SCAN fd = new SCAN(53, PriorityHandlingMethod.FDSCAN);
        assertEquals(1942, fd.simulate(createPriorityTestOrders()));
        assertEquals(1, fd.howManyPastDeadline());
    }

    @Test
    public void simpleCSCANTest() {
        //|65-53| + |67-65| + |98-67| + |122-98| + |124-122| + |183-124| + |1000-183| + 999 + |14-1| + |37-14| = 984
        assertEquals(1982, (new CSCAN(53)).simulate(createTestOrders()));
    }

    @Test
    public void arrivingCSCANTest() {
        assertEquals(2069, (new CSCAN(53)).simulate(createArrivingTestOrders()));
    }

    @Test
    public void sneakyCSCANTest() {
        //53i -> 300i -> 500i => 447
        assertEquals(447, (new CSCAN(53)).simulate(sneakyOrder()));
    }

    @Test
    public void EDFCSCANTest() {
        CSCAN edfCscan = new CSCAN(53, PriorityHandlingMethod.EDF);
        assertEquals(2580, edfCscan.simulate(createPriorityTestOrders()));
        assertEquals(2, edfCscan.howManyPastDeadline());
    }

    @Test
    public void FDSCANCSCANTest() {
        assertEquals(2450, (new CSCAN(53, PriorityHandlingMethod.FDSCAN)).simulate(createPriorityTestOrders()));
    }

    @Test
    public void noPriorityEDFTest() {
        assertEquals(
                (new FCFS(53)).simulate(createArrivingTestOrders()),
                (new FCFS(53, PriorityHandlingMethod.EDF)).simulate(createArrivingTestOrders())
        );
    }

    @Test
    public void noPriorityFDSCANTest() {
        assertEquals(
                (new SCAN(53)).simulate(createArrivingTestOrders()),
                (new SCAN(53, PriorityHandlingMethod.FDSCAN)).simulate(createArrivingTestOrders())
        );
    }

    private ArrayList<Order> createTestOrders() {
        ArrayList<Order> ordersQueue = new ArrayList<>();
        ordersQueue.add(new Order(98, 0));
        ordersQueue.add(new Order(183, 0));
        ordersQueue.add(new Order(37, 0));
        ordersQueue.add(new Order(122, 0));
        ordersQueue.add(new Order(14, 0));
        ordersQueue.add(new Order(124, 0));
        ordersQueue.add(new Order(65, 0));
        ordersQueue.add(new Order(67, 0));

        return ordersQueue;
    }

    private ArrayList<Order> createArrivingTestOrders() {
        ArrayList<Order> ordersQueue = new ArrayList<>();
        ordersQueue.add(new Order(64, 0));
        ordersQueue.add(new Order(12, 0));
        ordersQueue.add(new Order(789, 14));
        ordersQueue.add(new Order(98, 62));
        ordersQueue.add(new Order(149, 62));
        ordersQueue.add(new Order(4, 62));
        ordersQueue.add(new Order(105, 148));
        ordersQueue.add(new Order(56, 257));
        ordersQueue.add(new Order(124, 257));
        ordersQueue.add(new Order(984, 670));
        ordersQueue.add(new Order(55, 1000));

        return ordersQueue;
    }

    private ArrayList<Order> createPriorityTestOrders() {
        ArrayList<Order> queue = new ArrayList<>();
        queue.add(new Order(109, 0));
        queue.add(new Order(69, 0, 10));
        queue.add(new Order(54, 0, 30));
        queue.add(new Order(564, 23, 1000));
        queue.add(new Order(614, 25, 900));
        queue.add(new Order(314, 1050));
        queue.add(new Order(505, 1100));
        queue.add(new Order(813, 1200, 2000));
        queue.add(new Order(5, 1200));

        return queue;
    }

    private ArrayList<Order> sneakyOrder() {
        ArrayList<Order> queue = new ArrayList<>();
        queue.add(new Order(500, 0));
        queue.add(new Order(300, 1));

        return queue;
    }

}