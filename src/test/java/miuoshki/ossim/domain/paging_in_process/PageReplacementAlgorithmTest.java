package miuoshki.ossim.domain.paging_in_process;

import miuoshki.ossim.domain.Page;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class PageReplacementAlgorithmTest {

    @Test
    public void FIFOTest() {
        assertEquals(15, (new FIFO(3)).simulate(bookTestQueue()));
    }

    @Test
    public void OPTTest() {
        assertEquals(9, (new OPT(3)).simulate(bookTestQueue()));
    }

    @Test
    public void LRUTest() {
        assertEquals(12, (new LRU(3)).simulate(bookTestQueue()));
    }

    @Test
    public void ALRUTest() {
        assertEquals(11, (new ALRU(3)).simulate(bookTestQueue()));
    }

    private List<Page> bookTestQueue() {
        List<Page> testingQueue = Arrays.asList(
                new Page(7),
                new Page(0),
                new Page(1),
                new Page(2),
                new Page(0),
                new Page(3),
                new Page(0),
                new Page(4),
                new Page(2),
                new Page(3),
                new Page(0),
                new Page(3),
                new Page(2),
                new Page(1),
                new Page(2),
                new Page(0),
                new Page(1),
                new Page(7),
                new Page(0),
                new Page(1)
        );

        return new ArrayList<>(testingQueue);
    }

}